Ομάδα
1) Γιαννακόπουλος Παναγιώτης - 2615
2) Μαρκάκης Εμμανουήλ-Στέφανος - 3970
3) Μπίρμπο Ράντο - 3821

Ο κώδικας γράφτηκε, μεταγλωτίστηκε και αποσφαλματώθηκε σε λειτουργικό σύστημα Arch Linux με τα εξής στοιχεία: kernel-2.6.32.7, gcc-4.4.3.
Επίσης τα εκτελέσιμα αρχεία(shell) έτρεξαν κανονικά και χωρίς σφάλματα στον διογένη (μέσω ssh) αλλά δεν έγινε μεταγλώτιση και αποσφαλμάτωση του κώδικα στον διογένη.

Μαζί με τον κώδικα παρέχεται ένα προκατασκευασμένο "αρχείο του συστήματος", virtual_disk.dat, (εικονικός δίσκος) καθώς και ένα bash script, make.sh, με τις εντολές που
χρησιμοποιήθηκαν για την μεταγλώτιση του πηγαίου κώδικα και την δημιουργία της βιβλιοθήκης.

Ta apo pano me greeklish
O kodikas grafthke, metaglotisthke kai aposfalmato8hke se leitourgiko systhma Arch Linux me ta ekshs stoixeia: kernel-2.6.32.7, gcc-4.4.3.
Epishs ta ektelesima arxeia(shell) etreksan kanonika kai xoris sfalmata ston diogenh (meso ssh) alla den egine metaglotish kai aposfalmatosh tou kodika ston diogenh.

Mazi me ton kodika parexetai ena prokataskeuasmeno "arxeio tou systhmatos", virtual_disk.dat, (eikonikos diskos) ka8os kai ena bash script, make.sh, me tis entoles pou
xrhsimopoih8hkan gia thn metaglotish tou phgaiou kodika kai thn dhmiourgia ths biblio8hkhs.
