#!/bin/bash

gcc -c -Wall -Wextra block_service.c dir_service.c file_service.c
ar rc libmyfs.a block_service.o dir_service.o file_service.o
ranlib libmyfs.a
gcc -Wall -Wextra -o shell shell.c -lm -lmyfs -L.
