/*********************************************************************************
 *                                                                               *
 * Sto arxeio ayto ylopoioume to file service. Brisketai sto endiameso epipedo   *
 * tou file system mas, dhladh anamesa sto block service tou katotatou epipedou  *
 * kai sto anotero epipedo ton utilites. Yposthrizontai synarthseis gia kleisimo *
 * enos arxeiou, grapsimo kai diabasma se kai apo ena arxeio kai telos synarthsh *
 * pou epistrefei to mege8os tou arxeiou. H basikh tou leitourgia einai h        *
 * diekpaireosh ton aithseon prosbashs.                                          *
 *                                                                               *
 *********************************************************************************/

/* aparaithta includes */
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "fs_structs.h"

//orismos ton 0 kai 1 os false kai true antistoixa
#define FALSE 0
#define TRUE 1
//ena byte == 8 bits :)
#define ONE_BYTE 8

//dhloseis ton eksoterikon synarthseon tou block service ths biblio8hkhs
//etsi oste na mporesoume na tis xrhsimopoihsoume.
extern void read_block(int, char *);
extern void write_block(int, char *);
extern void buffer_to_blocks(char *, char[][the_superblock->block_size]);
extern int find_free_blocks(char *, int);

/* prototypa synarthseon */
int my_close(int);
int file_size(int);
int my_read(int, char *, int, int);
int my_write(int, char *, int, int);

/*
 * H synarthsh my_close(onomasthke etsi gia apofygh pi8anon conflicts
 * me thn synarthsh close tou unix) kleinei ena anoixto arxeio
 * kai epistrefei 0 se epityxia h -1 se apotyxia.
 */
int my_close(int ufid) {

	//an to ufid einai mesa sta epitrepta oria
	if(ufid >= 0 || ufid < the_superblock->num_of_inodes) {
		//an to arxeio einai anoixto
		if(inodes[ufid].file_is_open)
			inodes[ufid].file_is_open = FALSE;	//kleinei to arxeio
		return 0;	//epityxia
	}
	else
		return -1;	//apotyxia
}

/*
 * H synarthsh file_size epistrefei to mege8os tou arxeiou se bytes.
 */
int file_size(int ufid) {
	return inodes[ufid].file_size;
}

/*
 * H synarthsh my_write(onomasthke etsi gia apofygh pi8anon conflicts
 * me thn synarthsh write tou unix) antigrafei apo th 8esh pos tou
 * anoixtou arxeiou ufid, num ari8mo apo bytes apo to buf. Epistrefei ton
 * ari8mo ton bytes pou grafthkan. Me pos=file_size(ufid) kanoume append
 * sto arxeio. Na shmeiosoume edo oti h ulopoihsh ths my_write einai genikh
 * kai oxi periorismenh sta plaisia ths askhshs, dhladh douleuei gia opoiesdhpote
 * times tou pos kai oxi mono gia tis times 0 kai file_size(ufid).
 */
int my_write(int ufid, char *buf, int num, int pos) {
	//an to arxeio den einai anoixto typonoume analogo mhnyma kai epistrefoume
	if(inodes[ufid].file_is_open == FALSE) {
		printf("Error: file is not open!\n");
		return -2;
	}

	//an to pos pou do8hke einai megalutero apo to mege8os tou arxeiou typonoume analogo mhnyma kai epistrefoume
	if(pos > file_size(ufid)) {
		printf("Error: position out of bounds!\n");
		return -2;
	}

	//ta bytes pros eggrafh
	int bytes_to_be_written = num;

	//an ta bytes pou 8eloume na grapsoume einai megalytera h isa se ari8mo me to mhkos tou buffer
	if((unsigned)bytes_to_be_written >= strlen(buf))
		bytes_to_be_written -= 1;	//den apo8hkeuoume ton xarakthra neas grammhs '\r' (to enter pou pathse o xrhsths)

	//an bytes pros eggrafh einai mhden epistrefoume 0
	if(bytes_to_be_written == 0)
		return 0;

	//to megisto mege8os pou mporei na exei ena arxeio
	int max_file_size = NUM_OF_INDEXED_BLOCKS * the_superblock->block_size;
	//an ta bytes pros eggrafh einai perissotera apo to megisto mege8os enos arxeiou ta kanoume isa me ayto
	if(bytes_to_be_written > max_file_size)
		bytes_to_be_written = max_file_size;

	//dhlonoume enan char buffer me mege8os bytes_to_be_written
	char buffer[bytes_to_be_written];
	//mhdenizoume ton buffer
	bzero(buffer, sizeof(buffer));
	//antigafoume apo ton buf ston buffer bytes_to_be_written to plh8os chars
	strncpy(buffer, buf, bytes_to_be_written);

	//8etoume ta bytes pou grafthkan se 0
	int bytes_written = 0;
	//ypologizoume ta blocks pou apaitountai gia na apo8hkeutei to keimeno pros eggrafh
	int blocks_needed = ceil((double)(bytes_to_be_written) / the_superblock->block_size);
	//an ta blocks pou apaitountai einai perissotera apo NUM_OF_INDEXED_BLOCKS ta kanoume isa me ayta efoson
	//sthn periptosh pou exoume dhmiourgia neou arxeiou to megisto keimeno pou 8a grafei einai iso me
	//NUM_OF_INDEXED_BLOCKS se blocks.
	if(blocks_needed > NUM_OF_INDEXED_BLOCKS)
		blocks_needed = NUM_OF_INDEXED_BLOCKS;

	//dhlonoume ena char pinaka gia thn apo8hkeush ton 8eseon ton eleu8eron blocks pou blroume(an yparxoun eleu8era)
	char free_blocks[blocks_needed];
	//briskoume (an yparxoun) blocks_needed (pou einai to poly NUM_OF_INDEXED_BLOCKS se ari8mo) to plh8os
	//eleu8era blocks gia apo8hkeush tou arxeiou.
	int num_of_free_blocks_needed = find_free_blocks(free_blocks, blocks_needed);

	//an ta eleu8era dia8esima blocks einai ligotera apo ta apaitoumena epistrfoume -1 pou shmainei pos o "diskos" exei "gemisei"
	if(num_of_free_blocks_needed < blocks_needed) {
		return -1;
	}

	//dhlonoume enan char disdiastato pinaka gia apo8hkeush tou buffer se blocks
	char string_blocks[blocks_needed][the_superblock->block_size];
	int i;
	//mhdebizoume ola ta blocks
	for(i=0; i<blocks_needed; i++)
		memset(string_blocks[i], 0, sizeof(string_blocks[i]));

	//o deikths tou block sto opoio 8a ksekinhsei h eggrafh
	int block_index;
	//analoga me to pos 8etoume to block_index
	if(((pos+1) % the_superblock->block_size) == 0)
		block_index = floor((double)pos / the_superblock->block_size);
	else
		block_index = floor((double)(pos+1) / the_superblock->block_size);
	if(block_index == NUM_OF_INDEXED_BLOCKS)
		return 0;
	//h 8esh tou char mesa sto block apo ton opoio 8a ksekinhsei h eggrafh
	int pos_of_char = pos % the_superblock->block_size;
	//h 8esh tou teleutaio xarakthra tou arxeiou mesa sto (teleutaio tou) block dedomenon
	int pos_of_last_char = (inodes[ufid].file_size-1) % the_superblock->block_size;
	if(inodes[ufid].file_size == 0)
		pos_of_last_char = 0;

	//dhlonoume ena char pinaka iso me to mege8os enos block
	char block[the_superblock->block_size];
	//mhdenizoume to block
	bzero(block, sizeof(block));

	//o deikths tou teleutaiou block dedomenon tou arxeiou
	int last_block_with_data;
	//ypologizoume ton deikth tou teleutaiou block dedomenon tou arxeiou
	last_block_with_data = floor((double)(inodes[ufid].file_size-1) / the_superblock->block_size);
	//an to arxeio exei mhdeniko mege8os tote o deikths tou teleutaiou block dedomenon tou arxeiou ginetai 0 (0->NUM_OF_INDEXED_BLOCKS-1)
	if(inodes[ufid].file_size == 0)
		last_block_with_data = 0;
	//o ari8mos ton eleu8eron deiktodotoumenon block, dhladh osa exoun timh -1 kai oxi th 8esh kapoiou block sto "disko"
	int num_of_free_indexed_blocks;
	//analoga me to last_block_with_data 8etoume to num_of_free_indexed_blocks
	if(last_block_with_data == 0)
		num_of_free_indexed_blocks = 12;
	else
		num_of_free_indexed_blocks = NUM_OF_INDEXED_BLOCKS - last_block_with_data - 1;

	//enas char buffer isos me blocks_needed*the_superblock->block_size+100 se mege8os
	char temp_buf[blocks_needed*the_superblock->block_size+100];
	//mhdenismos tou buffer
	bzero(temp_buf, sizeof(temp_buf));

	//an o deikths tou block sto opoio 8a ksekinhsei h eggrafh isoutai me ton deikth tou teleutaiou block dedomenon tou arxeiou
	//kai to arxeio den exei mhdeniko mege8os
	if(block_index == last_block_with_data && inodes[ufid].file_size > 0) {
		read_block(inodes[ufid].indexed_blocks[block_index], block);	//diabase mono to teleutaio block dedomenon tou arxeiou
		strcat(temp_buf, block);	//senenose to me ton temp_buf
	}
	else {//alios
		//metablhth gia ton ka8orismo mexri pou na diabasoume sto arxeio
		int read_up_to;
		//an o deikths tou block sto opoio 8a ksekinhsei h eggrafh + ta apaitoumena blocks einai mikrotera
		//h isa me ton deikth tou teleutaiou block dedomenon tou arxeiou kane to read_up_to = block_index+blocks_needed
		if(block_index+blocks_needed <= last_block_with_data)
			read_up_to = block_index+blocks_needed;
		else//allios kane to read_up_to = last_block_with_data
			read_up_to = last_block_with_data;
		//apo to indexed_blocks[block_index] mexri kai to indexed_blocks[read_up_to-1]
		for(i=block_index; i<read_up_to; i++) {
			// diabazoume to arxeio(string) block by block kai to apo8hkeuoume ston pinaka block
			read_block(inodes[ufid].indexed_blocks[i], block);
			//synenonoume ton block me ton temp_buf
			strcat(temp_buf, block);
		}
	}

	//antigrafoume apo th 8esh pos_of_char mexri kai th 8esh pos_of_char+bytes_to_be_written-1 ta periexomena tpu buffer ston temp_buf
	int j = 0;
	for(i=pos_of_char; i<pos_of_char+bytes_to_be_written; i++) {
		temp_buf[i] = buffer[j++];
	}

	//spame to temp_buf se blocks isa me to mege8os tou block tou file system mas
	buffer_to_blocks(temp_buf, string_blocks);

	//an h 8esh apo thn opoia 8eloume na grapsoume + ton ari8mo ton bytes pou einai pros eggrafh einai mikrotera
	//h isa apo to mege8os tou arxeiou ayty shmainei pos 8a grafoun ola.
	int k;
	if((unsigned)pos + (unsigned)bytes_to_be_written <= inodes[ufid].file_size) {
		j = 0;
		k = 0;
		for(i=block_index; i<block_index+blocks_needed; i++) {
			// grafoume to arxeio(string) block by block
			write_block(inodes[ufid].indexed_blocks[i], string_blocks[k++]);
		}
		//ypologizoume ta bytes pou grafthkan
		bytes_written = bytes_to_be_written;
	}
	else {//allios an einai perissotera apo to mege8os tou arxeiou, pou shmainei pos den 8a grafoun ola
		//ypologizoume posa bytes ekteinontai pera apo to mege8os tou arxeiou
		int x = pos + bytes_to_be_written - inodes[ufid].file_size;
		//blocks pou apaitountai gia ta epipleon ayta bytes
		int y;
		//ypologizoume posa blocks apaitountai gia ta epipleon ayta bytes
		y = ceil((double)x / the_superblock->block_size);
		int extra_block = 0;
		if(((unsigned)pos == inodes[ufid].file_size) && (pos_of_last_char < (the_superblock->block_size-1))
			&& (pos != 0) && (x > (the_superblock->block_size-1-pos_of_last_char)))
			extra_block = 1;

		//an ta epipleon blocks einai ligotera h isa apo ta eleu8era deiktodotoumena blocks
		if(y <= num_of_free_indexed_blocks) {
			j = 0;
			k = 0;
			for(i=block_index; i<block_index+blocks_needed+extra_block; i++) {
				//an prepei na grapsoume se neo block pou den deixnei se kapoio block tou diskou
				if(inodes[ufid].indexed_blocks[i] == -1) {
					//kanoume to neo ayto block na deixnei se ena eleu8ero block tou "diskou", pou 8a
					//desmeytei molis graftei to block sto "disko".
					inodes[ufid].indexed_blocks[i] = free_blocks[j++];
				}
				// grafoume to arxeio(string) block by block
				write_block(inodes[ufid].indexed_blocks[i], string_blocks[k++]);
			}
			//ypologizoume ta bytes pou grafthkan
			bytes_written = bytes_to_be_written;
			//ypologizoume to neo mege8os tou arxeiou
			inodes[ufid].file_size += x;
		}
		//allios an ta epipleon blocks einai perissotera apo ta eleu8era deiktodotoumena blocks
		else if(y > num_of_free_indexed_blocks) {
			//metablhth gia thn katametrhsh ton sunolikon blocks pou grafontai
			int blk_written_counter = 0;
			//metablhth gia thn katametrhsh ton blocks pou grafontai meta apo to teleutaio
			//block dedomenon tou arxeiou
			int blk_written_past_last_counter = 0;
			j = 0;
			k = 0;
			for(i=block_index; i<NUM_OF_INDEXED_BLOCKS; i++) {
				//an ftasame sto epomeno apo to teleutaio block dedomenon tou arxeiou
				if(i >= last_block_with_data+1)
					blk_written_past_last_counter++;	//aykshse ton counter kata 1
				//an prepei na grapsoume se neo block pou den deixnei se kapoio block tou diskou
				if(inodes[ufid].indexed_blocks[i] == -1) {
					//kanoume to neo ayto block na deixnei se ena eleu8ero block tou "diskou", pou 8a
					//desmeytei molis graftei to block sto "disko".
					inodes[ufid].indexed_blocks[i] = free_blocks[j++];
				}
				// grafoume to arxeio(string) block by block
				write_block(inodes[ufid].indexed_blocks[i], string_blocks[k++]);
				//ena neo block grafthke, aykshse ton counter kata 1
				blk_written_counter++;
			}
			//ypologizoume ton ari8mo ton bytes pou grafthkan
			if(((unsigned)pos == inodes[ufid].file_size) && (inodes[ufid].file_size < NUM_OF_INDEXED_BLOCKS*the_superblock->block_size))
				bytes_written = (blk_written_past_last_counter * the_superblock->block_size) + (the_superblock->block_size-1-pos_of_last_char);
			else if(((unsigned)pos == inodes[ufid].file_size) && (x <= the_superblock->block_size-1-pos_of_last_char))
				bytes_written = x;
			else
				bytes_written = (blk_written_counter * the_superblock->block_size) - pos_of_char;
			//ypologizoume to neo mege8os tou arxeiou
			if(inodes[ufid].file_size < NUM_OF_INDEXED_BLOCKS*the_superblock->block_size && (x > the_superblock->block_size-1-pos_of_last_char))
				inodes[ufid].file_size += (blk_written_past_last_counter * the_superblock->block_size) + (the_superblock->block_size-1-pos_of_last_char);
			else if(inodes[ufid].file_size < NUM_OF_INDEXED_BLOCKS*the_superblock->block_size && (x <= the_superblock->block_size-1-pos_of_last_char))
				inodes[ufid].file_size += x;
		}
	}

	return bytes_written;
}

/*
 * H synarthsh my_read(onomasthke etsi gia apofygh pi8anon conflicts
 * me thn synarthsh read tou unix) antigrafei apo th 8esh pos tou
 * anoixtou arxeiou ufid, num ari8mo apo bytes mesa sto buf. Epistrefei ton
 * ari8mo ton bytes pou diabasthkan(mporei na einai mikroteros apo to num an exei
 * ftasei sto telos tou arxeiou). Na shmeiosoume edo oti h ulopoihsh ths my_read
 * einai genikh kai oxi periorismenh sta plaisia ths askhshs, dhladh douleuei gia
 * opoiesdhpote times tou pos.
 */
int my_read(int ufid, char *buf, int num, int pos) {

	//elegxoume an to arxeio einai anoixto
	if(inodes[ufid].file_is_open == FALSE) {
		printf("Error: file is not open!\n");
		return -2;
	}

	//an to arxeio einai adeio epistrefopume 0 bytes
	if(file_size(ufid) == 0)
		return 0;

	//allios an to pos pou do8hke gia diabasma einai megalutero apo ton teleytaio xarakthra tou arxeiou
	//epistrefoume thn timh -2 pou shmatodotei sfalma.
	if(pos >= file_size(ufid)) {
		printf("Error: position out of bounds!\n");
		return -2;
	}

	//ta bytes pou diabasthkan synolika
	int bytes_read = 0;
	//ta apaitoumena blocks gia thn apouhkeush tou arxeiou
	int blocks_needed = ceil((double)(num) / the_superblock->block_size);
	//o deikths tou block sto opoio periexetai to pos pou do8hke os orisma
	int block_index = floor((double)pos / the_superblock->block_size);
	//h 8esh tou xarakthra mesa sto block apo ton opoio 8a ksekinhsei to diabasma tou arxeiou
	int pos_of_char = pos % the_superblock->block_size;

	//dhlonoume ena pinaka char me mege8os oso ena block gia na diabazoume ta blocks tou arxeiou
	char block[the_superblock->block_size];
	//mhdenixoume to block
	bzero(block, sizeof(block));

	//enas buffer gia apou8hkeysh olou tou periexomenou tou arxeiou pou prokeitai na diabastei
	char buffer[blocks_needed*the_superblock->block_size];
	//mhdenizoume ton buffer
	bzero(buffer, sizeof(buffer));

	int i;
	for(i=block_index; i<block_index+blocks_needed; i++) {
		// diabazoume to arxeio(string) block by block
		read_block(inodes[ufid].indexed_blocks[i], block);
		//synenonoume to block pou molis diabasame ston buffer
		strcat(buffer, block);
	}

	int read_up_to;
	//an to a8roisma pos_of_char+num einai mikrotero apo to mege8os tou arxeiou
	if(pos_of_char+num <= file_size(ufid))
		read_up_to = pos_of_char+num;	//diabazoume mexri ton xarakthra pos_of_char+num
	else
		read_up_to = file_size(ufid);	//allios diabazoume mexri to telos tou arxeiou

	//antigrafoume apo th 8esh pos_of_char mexri th 8esh read_up_to ta periexomena tou  buffer mesa ston buf
	int j = 0;
	for(i=pos_of_char; i<read_up_to; i++) {
		buf[j++] = buffer[i];
		bytes_read++;	//metrame ta bytes pou diabazontai
	}

	return bytes_read;
}
