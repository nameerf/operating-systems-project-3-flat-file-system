/******************************************************************************
 *                                                                            *
 * Sto arxeio ayto ylopoioume to ypotypodes kelyfos (shell). Brisketai sto    *
 * ypshlotero epipedo sto opoio exei prosbash o xrhsths kai sto opoio mporei  *
 * na eisagei entoles(utilities) tis opoies to kelyfos metatrepei se klhseis  *
 * synarthseon ton Directory kai File Services pou briskontai sto endiameso   *
 * epipedo, oi opoies me th seira tous kaloun tis xamhloterou epipedou        *
 * synarthseis tou Block Service pou analambanoun th metakinhsh block apo kai *
 * pros ton eikoniko disko. Ta utilities exoun ensomato8ei sto kelyfos os     *
 * synarthseis.                                                               *
 *                                                                            *
 ******************************************************************************/

/* aparaithta includes */
#include <ctype.h>
#include <fcntl.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>

#include "fs_structs.h"

//edo orizoume to to onoma tou "arxeiou tou systhmatos" (eikonikou diskou)
#define NAME_OF_VIRTUAL_DISK "virtual_disk.dat"

//orismos ton 0 kai 1 os false kai true antistoixa
#define FALSE 0
#define TRUE 1
//ena byte == 8 bits :)
#define ONE_BYTE 8
//ena arketa megalo (megisto) mhkos gia thn entolh pou eisagei o xrhsths
#define MAX_COMMAND_LENGTH 100
//ena arketa megalo (megisto) mhkos gia thn ka8e parametro pou eisagei o xrhsths
#define MAX_PARAMETER_LENGTH 50
//tomegisto plh8os ton parametron ka8e entolhs
#define MAX_PARAMETERS 2
//to synoliko plh8os ton entolon pou uposthrize to kelufos
#define NUM_OF_COMMANDS 12

//dhloseis ton eksoterikon synarthseon tou block service ths biblio8hkhs
//etsi oste na mporesoume na tis xrhsimopoihsoume.
extern int files(char **);
extern int create(const char *);
extern int delete(const char *);
extern int my_open(const char *);
extern int my_close(int);
extern int my_write(int, char *, int, int);
extern int my_read(int, char *, int, int);
extern int file_size(int);
extern int number_of_free_blocks(void);

/* prototypa synarthseon */
void type_prompt(void);
void read_command(char [], char [MAX_PARAMETERS][MAX_PARAMETER_LENGTH], int *);

void mkfs(int, char *[]);
void mount(void);
void umount(void);
void quit(void);
void help(void);
void ls(void);
void cp(int, char *[]);
void mv(int, char *[]);
void rm(int, char *[]);
void df(void);
void echo(int, char *[]);
void cat(int, char *[]);

//enas buffer gia thn apo8hkeush ths eisodou tou xrhsth sto kelufos
char buffer[500];
//global metablhth pou ti8etai se true otan to file system ginetai mounted
int mounted = FALSE;
//global metablhth pou ti8etai se true otan to file system ginetai unmounted
int unmounted = FALSE;

int main(void) {

	//metablhth gia to an exoume egkurh entolh
	int valid_command = FALSE;

	//oi ka8orismenes entoles pou yposthrizei to kelufos
	const char *available_commands[] = {"mkfs", "mount", "umount", "quit", "help", "ls", "cp", "mv", "rm", "df", "cat", "echo"};

	//deikths se char gia thn entolh
	char *command;
	//pinakas me deiktes se chars gia tis parametrous
	char *parameters[MAX_PARAMETERS];

	//orizoume ena pinaka me chars me mege8os iso me MAX_COMMAND_LENGTH
	char com[MAX_COMMAND_LENGTH];
	//orizoume ena disdiastato pinaka me MAX_PARAMETERS grammes me chars me mege8os ka8e grammhs iso me MAX_PARAMETER_LENGTH
	char par[MAX_PARAMETERS][MAX_PARAMETER_LENGTH];

	int x = 0;
	//dhlonoume ena deikth gia thn katametrhsh ton parametron pou do8hkan poato xrshsth
	int *num_of_par;
	//o deikths deixnei sthn x pou einai 0
	num_of_par = &x;

	while(TRUE) {//atermon brogxos
		//emfanish tou prompt tou shell
		type_prompt();

		//diabazoume thn entolh pou edose o xrhsths mazi me tis opoies parametrous
		read_command(com, par, num_of_par);

		//kanoume ton deikth command na deixne ston pinaka com pou periexei thn entolh tou xrhsth
		command = com;

		int i;
		for(i=0; i<MAX_PARAMETERS; i++) {
			parameters[i] = NULL;
			if(i < *num_of_par)
				parameters[i] = par[i]; //kanoume tous deiktes parameters[i] na deixnon stous pinakes par[i] pou periexoun tis parametrous
		}

		//ksana8etoume thn valid_command se FALSE
		valid_command = FALSE;
		//elegxoume an h entolh pou edose o xrhsths brisketai anamesa stis entoles pou yposthrizei to shell
		for(i=0; i<NUM_OF_COMMANDS; i++) {
			if(strcmp(available_commands[i], command) == 0) {
				valid_command = TRUE;	//egkurh entolh
				break;
			}
		}

		if(!valid_command) {	//mh egkurh entolh, synexizoume
			printf("Unknown command!\n");
			continue;
		}

		//an h entolh pou edose o xrhsths einai quit kaloume thn synarthsh-utility quit kai synexizoume
		if(strcmp("quit", command) == 0) {
			quit();
			continue;
		}

		//an h entolh pou edose o xrhsths einai help kaloume thn synarthsh-utility help kai synexizoume
		if(strcmp("help", command) == 0) {
			help();
			continue;
		}

		//an h entolh pou edose o xrhsths einai mkfs kaloume thn synarthsh-utility mkfs me tis dosmenes parametrous kai synexizoume
		if(strcmp("mkfs", command) == 0) {
			mkfs(*num_of_par, parameters);
			continue;
		}

		//an h entolh pou edose o xrhsths einai mount kaloume thn synarthsh-utility mount kai synexizoume
		if(strcmp("mount", command) == 0) {
			mount();
			continue;
		}

		if(mounted) {	//elegxoume an to file system mas einai mounted, opote kai mporoun na do8oun alles entoles
			//an h entolh pou edose o xrhsths einai ls kaloume thn synarthsh-utility ls kai synexizoume
			if(strcmp("ls", command) == 0) {
				ls();
				continue;
			}
			//an h entolh pou edose o xrhsths einai cp kaloume thn synarthsh-utility cp me tis dosmenes parametrous kai synexizoume
			if(strcmp("cp", command) == 0) {
				cp(*num_of_par, parameters);
				continue;
			}
			//an h entolh pou edose o xrhsths einai mv kaloume thn synarthsh-utility mv me tis dosmenes parametrous kai synexizoume
			if(strcmp("mv", command) == 0) {
				mv(*num_of_par, parameters);
				continue;
			}
			//an h entolh pou edose o xrhsths einai rm kaloume thn synarthsh-utility rm me tis dosmenes parametrous kai synexizoume
			if(strcmp("rm", command) == 0) {
				rm(*num_of_par, parameters);
				continue;
			}
			//an h entolh pou edose o xrhsths einai df kaloume thn synarthsh-utility df kai synexizoume
			if(strcmp("df", command) == 0)  {
				df();
				continue;
			}
			//an h entolh pou edose o xrhsths einai cat kaloume thn synarthsh-utility cat me tis dosmenes parametrous kai synexizoume
			if(strcmp("cat", command) == 0) {
				cat(*num_of_par, parameters);
				continue;
			}
			//an h entolh pou edose o xrhsths einai echo kaloume thn synarthsh-utility echo me tis dosmenes parametrous kai synexizoume
			if(strcmp("echo", command) == 0) {
				echo(*num_of_par, parameters);
				continue;
			}
			//an h entolh pou edose o xrhsths einai umount kaloume thn synarthsh-utility umount kai synexizoume
			if(strcmp("umount", command) == 0) {
				umount();
				continue;
			}
		}
		else {	//an to file system den einai mounted yponoume analogo mhnyma
			printf("Command cannot execute because the file system isn't mounted.\n");
			printf("Mount the file system with the 'mount' command and retry.\n");
		}
	}

	return 0;
}

/*
 * H synarthsh type_prompt emfanizei to prompt tou shell.
 */
void type_prompt(void) {
	printf("[myfs shell]# ");
	fflush(stdout);
}

/*
 * H synarthsh read_command diabazei thn eisodo tou xrhsth. Dexetai 3 parametrous,
 * enan char pinaka gia thn apo8hkeush ths entolhs tou xrhsth, enan char disdiastato
 * pinaka gia thn apo8hkeush ton parametron pou eisagei o xrhsths kai enan deikth se int
 * pou metra to plh8os ton parametron pou eisagei o xrhsths. Den epistrefei tipota.
 */
void read_command(char com[], char param[MAX_PARAMETERS][MAX_PARAMETER_LENGTH], int *nop) {

	//apouhkeuei thn eisodo tou xrhsth mesa ston buffer
	fgets(buffer, 500, stdin);

	//mia metablhth pou elegxei an exei oloklhro8ei h antigrafh ths entolhs apo ton buffer ston com
	int command_completed = FALSE;
	int j = 0;
	int k = 0;
	int m = 0;
	unsigned int i;
	for(i=0; i<strlen(buffer); i++) {
		if(!command_completed) //oso h entolh den exei oloklhro8ei(dhl den exoume synanthsei space)
			com[j++] = buffer[i];	//antegrapse to buffer ston com
		if(i == strlen(buffer)-1 && !command_completed) {	//exoume mono mia entolh xoris spaces(parametrous)
			com[j-1] = '\0';	//kleisimo tou string ths entolhs me ton eidiko xarakthra termatismou string
			break;
		}
		if(buffer[i] == ' ' && !command_completed) {
			com[j-1] = '\0';	//kleisimo tou string ths entolhs me ton eidiko xarakthra termatismou string
			command_completed = TRUE;	//teleiose h antigrafh ths entolh (synanthsame space)
			i++;
		}
		//an exei teleiosei h antigrafh ths entolhs (synanthsame space) kai exoume tis sostes sto plh8os parametrous
		if(command_completed && m < MAX_PARAMETERS) {
			//antigafoume to buffer sthn antistoixh parametro
			param[m][k++] = buffer[i];
			if(buffer[i] == ' ') {	//an synathsame keno phgaine sthn epomenh parametro
				param[m][k-1] = '\0';	//kleisimo tou string ths parametrou me ton eidiko xarakthra termatismou string
				m++;
				k = 0;
			}
			if(i == strlen(buffer)-1) {	//an ftasame sto telos tou buffer, "kleise" thn teleutaia entolh kai bges
				param[m][k-1] = '\0';	//kleisimo tou string ths parametrou me ton eidiko xarakthra termatismou string
				break;
			}
		}
	}

	//apo8hkeuoume ton antistoixo ari8mo parametron
	if(command_completed)
		*nop = m+1;
	else
		*nop = 0;
}

/*
 * H synarthsh-utility mkfs dhmiourgei to arxeio tou systhmatos kai arxikopoiei to
 * systhma arxeion (eggrafh ton default timon tou superblock). Epishs einai ypey8ynh
 * kai gia thn eggrafh ton basikon domon pou elegxoun th leitourgia tou systhmatos
 * arxeion ston "disko". An to "arxeio tou systhmatos"("diskos") exei hdh
 * arxikopoih8ei, tote mhdenizetai kai arxikopoieitai ek neou. Dexetai dyo orismata,
 * to proto einai o ari8mos ton parametron pou edose o xrhsths kai to deytero einai
 * enas pinakas me deiktes se chars pou periexei tis parametrous(blocks kai block-size)
 * pou edose o xrhsths. H parametros blocks orizei ton ari8mo ton blocks pou prosferei
 * to systhma arxeion kai h parametros block-size orizei to mege8os ton blocks.
 */
void mkfs(int argc, char *argv[]) {
	//an o ari8mos ton parametron einai diaforos tou 2, typose analogo mhnyma kai epestrepse
	if(argc != 2) {
		printf("Invalid number of parameters!\n");
		return;
	}

	//metablhtes gia na elenksoume thn egkyrothta ton parametron pou edose o xrhsths
	int valid_arg1 = TRUE;
	int valid_arg2 = TRUE;
	unsigned int k;
	//elegxoume an oi parametroi pou edose o xrhsths gia ta blocks kai block-size periexoun mono ari8mous
	for(k=0; k<strlen(argv[0]); k++) {
		if(!isdigit(argv[0][k])) {
			valid_arg1 = FALSE;
			break;
		}
	}
	for(k=0; k<strlen(argv[1]); k++) {
		if(!isdigit(argv[1][k])) {
			valid_arg2 = FALSE;
			break;
		}
	}
	//an kapoia apo tis parametrous(h kai oi 2) den einai egkurh typose analogo mhnyma kai epestrepse
	if(valid_arg1 == FALSE || valid_arg2 == FALSE) {
		printf("You must enter only digits for the number of blocks and the size of each block!\n");
		return;
	}

	//allios an einai kai oi duo egkures apo8hkeuse tes stis antistoixes metablhtes, number_of_blocks kai block_size
	int number_of_blocks = atoi(argv[0]);
	int block_size = atoi(argv[1]);

	//SHMEIOSH: epeidh h metablhth indexed_blocks ths domhs tou i-node(blepe fs_structs) exei dhlo8ei os short 8etoume
	//os ano orio gia ton ari8mo ton blocks pou mporei na eisagei o xrhsths thn sta8era SHRT_MAX etsi oste h timh tou ka8e
	//indexed_block na mhn yperbainei to orio. 8eorhsame oti kati tetoio den allionei thn ylopoihsh afou den ftiaxnoume kati
	//entelos realistiko kai gia tous ekpaideytikous skopous ths askhshs einai arketo. Epishs 8etoume kai ena kato orio
	//tetoio oste na mporoun na apo8hkeytoun oi basikes domes tou file system xoris problhmata. An h timh pou dinei o xrhsths
	//den einai mesa sta epitrepta oria typonoume analogo mhnyma kai epistrefoume.
	if(number_of_blocks < 150 || number_of_blocks > SHRT_MAX) {
		printf("You must enter a number that is between 150 and %u for the number of blocks of the file system.\n", SHRT_MAX);
		return;
	}
	//Epishs kai gia to mege8os tou block exei te8ei ena kato orio etsi oste to superblock na xoraei se ena mono block. Den exoume
	//8esei kapoio ano orio kai 8eoroume pos o xrhsths /bazei logikes times. An h timh pou dinei o xrhsths den einai mesa sta
	//epitrepta oria typonoume analogo mhnyma kai epistrefoume.
	if(block_size < 32) {
		printf("You must enter a number that is greater or equal to 32 for the size of each block.\n");
		return;
	}

	//diairoume thn timh pou edose o xrhsths gia to mege8os tou block me to mege8os ths domhs tou i-node kai to apo8hkeuoume
	//sthn metablhth block_size_factor.
	int block_size_factor = block_size / sizeof(i_node);
	//pollaplasiazoume to block_size_factor me to mege8os ths domhs tou i-node. Ayto to kanoume etsi oste se ena block na xoraei
	//ena akeraio plh8os apo i-nodes. Dedomenou oti to mege8os ths domhs tou i-node einai 32 bytes den yparxei megalh apoklish
	//apo thn timh pou dinei o xrhsths.
	block_size = block_size_factor * sizeof(i_node);
	printf("Size of each block is 'rounded' to %d\n", block_size);

	//metablhth gia thn apo8hkeysh ton megiston arxeion pou mporei na yposthriksei to file system mas.
	int max_num_of_files = 0;
	//metablhtes gia to posa blocks xreiazontai gia thn ka8e domh
	int blocks_needed_for_bbm = 0;
	int blocks_needed_for_ibm = 0;
	int blocks_needed_for_dt = 0;
	int blocks_needed_for_ind = 0;
	//ta blocks ton dedomenon
	int blocks_needed_for_data = 0;
	//ta synolika blocks tou file system mas
	int total_blocks_needed = 0;


	//Parakato, analoga me tis times pou dinei o xrhsths gia ta blocks kai block-size, ypologizoume dunamika ta apaitoumena
	//blocks pou xreiazontai gia thn apo8hkeush ton domon ston "disko" ka8os kai to megisto plh8os i-nodes(arxeion) me mono
	//dedomeno ton megisto ari8mo blocks(NUM_OF_INDEXED_BLOCKS) pou deiktodotei to ka8e i-node. Me ton tropo ayto ftanoume
	//se mia timh pou proseggixei ikanopoihtika(xoris megalh apoklish) ton ari8mo ton blocks pou edose o xrhsths.
	int i;
	for(i=10; ; i++) {
		if(total_blocks_needed >= number_of_blocks) {
			max_num_of_files = i-1;
			break;
		}
		blocks_needed_for_bbm = ceil((double)(sizeof(char) * ceil((double)(blocks_needed_for_data) / ONE_BYTE)) / block_size);
		blocks_needed_for_ibm = ceil((double)(sizeof(char) * ceil((double)(i) / ONE_BYTE)) / block_size);
		blocks_needed_for_dt = ceil((double)(sizeof(directory_table) * i) / block_size);
		blocks_needed_for_ind = ceil((double)(sizeof(i_node) * i) / block_size);
		blocks_needed_for_data = i * NUM_OF_INDEXED_BLOCKS;
		total_blocks_needed = 1 + blocks_needed_for_bbm + blocks_needed_for_ibm + blocks_needed_for_dt + blocks_needed_for_ind + blocks_needed_for_data;
	}
	printf("Total blocks of the file system: %d\n", total_blocks_needed);

	//apo8hkeuoume to plh8os ton stoixeion tou pinaka block_bit_map
	int bbm_elements = ceil((double)(blocks_needed_for_data) / ONE_BYTE);
	//metablhth gia to posa blocks 8a ginoun allocated sto teleutaio stoixeio tou block bit map
	int blocks_to_mark_as_allocated = 0;
	//an ta blocks_needed_for_data den diairountai akribos me to ONE_BYTE 8etoume analoga ta blocks_to_mark_as_allocated
	if(blocks_needed_for_data % ONE_BYTE != 0)
		blocks_to_mark_as_allocated = (bbm_elements * ONE_BYTE) - blocks_needed_for_data;
	//apo8hkeuoume to plh8os ton stoixeion tou pinaka inodes_bit_map
	int ibm_elements = ceil((double)(max_num_of_files) / ONE_BYTE);
	//metablhth gia to posa i-nodes 8a ginoun binded sto teleutaio stoixeio tou i-nodes bit map
	int inodes_to_mark_as_binded = 0;
	//an ta max_num_of_files den diairountai akribos me to ONE_BYTE 8etoume analoga ta inodes_to_mark_as_binded
	if(max_num_of_files % ONE_BYTE != 0)
		inodes_to_mark_as_binded = (ibm_elements * ONE_BYTE) - max_num_of_files;

	//dhlonoume tis basikes domes tou file system
	superblock *sb = NULL;
	unsigned char bbm[bbm_elements];
	unsigned char ibm[ibm_elements];
	directory_table dt[max_num_of_files];
	i_node ind[max_num_of_files];

	//arxikopoioume to block bit map me thn timh 0 se ka8e 8esh tou pinaka pou shmainei oti arxika ola ta blocks
	//ton dedomenon einai eley8era.
	for(i=0; i<bbm_elements; i++) {
		bbm[i] = 0;
	}
	//maska me tin opoia desmeuoume sigkekrimena bits sta blocka kai i-nodes bit maps
	unsigned int allocate_bits;
	switch(blocks_to_mark_as_allocated) {
		case 1: allocate_bits = 128; break;		//allocate_bits=!0000000
		case 2: allocate_bits = 192; break;		//allocate_bits=11000000
		case 3: allocate_bits = 224; break;		//allocate_bits=11100000
		case 4: allocate_bits = 240; break;		//allocate_bits=11110000
		case 5: allocate_bits = 248; break;		//allocate_bits=11111000
		case 6: allocate_bits = 252; break;		//allocate_bits=11111100
		case 7: allocate_bits = 254; break;		//allocate_bits=11111110
	}
	//efoson grapsame thn zhtoumenh timh sthn maska allocate_bits, thn efrmozoume sto block bit map
	//etsi oste to kainourgio block bit map pou prokuptei na exei kai ta zhtoumena bits desmeymena (logiko 1)
	bbm[bbm_elements-1] |= allocate_bits;
	//bbm[10] = 0;
	//bbm[11] = 15;
	//bbm[12] = 0;
	//bbm[13] = 15;

	//arxikopoioume to i-nodes bit map me thn timh 0 se ka8e 8esh tou pinaka pou shmainei oti arxika den exoume
	//ka8olou arxeia.
	for(i=0; i<ibm_elements; i++) {
		ibm[i] = 0;
	}
	switch(inodes_to_mark_as_binded) {
		case 1: allocate_bits = 128; break;		//allocate_bits=!0000000
		case 2: allocate_bits = 192; break;		//allocate_bits=11000000
		case 3: allocate_bits = 224; break;		//allocate_bits=11100000
		case 4: allocate_bits = 240; break;		//allocate_bits=11110000
		case 5: allocate_bits = 248; break;		//allocate_bits=11111000
		case 6: allocate_bits = 252; break;		//allocate_bits=11111100
		case 7: allocate_bits = 254; break;		//allocate_bits=11111110
	}
	//efoson grapsame thn zhtoumenh timh sthn maska allocate_bits, thn efrmozoume sto i-nodes bit map
	//etsi oste to kainourgio i-nodes bit map pou prokuptei na exei kai ta zhtoumena bits desmeymena (logiko 1)
	ibm[ibm_elements-1] |= allocate_bits;

	//arxikopoihsh tou directory table
	for(i=0; i<max_num_of_files; i++) {
		strcpy(dt[i].filename, "");		//keno onoma
		dt[i].inode_index = -1;			//den yparxei antistoixo i-node
	}

	//arxikopoihsh ton i-nodes
	int j;
	for(i=0; i<max_num_of_files; i++) {
		ind[i].file_size = -1;		//mege8os = -1
		ind[i].file_is_open = FALSE;	//arxeio kleisto
		for(j=0; j<NUM_OF_INDEXED_BLOCKS; j++)
			ind[i].indexed_blocks[j] = -1;	//oi indexes den deixnoun se kanena egkuro block dedomenon
	}

	//desmeush mnhmhs gia to superblock
	sb = (superblock*)malloc(sizeof(superblock));
	//eggrafh ton timon tou superblock me times pou ypologisame parapano
	sb->block_size = block_size;
	sb->total_blocks = total_blocks_needed;
	sb->used_blocks_for_block_bmap = blocks_needed_for_bbm;
	sb->bbmap_elements = bbm_elements;
	sb->used_blocks_for_inodes_bmap = blocks_needed_for_ibm;
	sb->ibmap_elements = ibm_elements;
	sb->used_blocks_for_dir_table = blocks_needed_for_dt;
	sb->used_blocks_for_inodes = blocks_needed_for_ind;
	sb->number_of_data_blocks = blocks_needed_for_data;
	sb->num_of_inodes = max_num_of_files;
	sb->data_start_location = total_blocks_needed - blocks_needed_for_data;

	//"anoigma" tou "diskou" mono gia angnosh, an to arxeio yparxei mhdenizetai to mege8os tou
	int fd = open(NAME_OF_VIRTUAL_DISK, O_WRONLY | O_TRUNC);

	printf("Please wait while the file system is being created and initialized...\n");
	int ret = ftruncate(fd, total_blocks_needed * block_size);
	if(ret == -1) {
		perror("ftruncate");
		exit(-1);
	}

	//metaferoume thn "kefalh" diskou sthn arxh tou
	lseek(fd, 0L, SEEK_SET);
	//grafoume to suberblock to opoio katalambanei ena block(to proto)
	write(fd, sb, block_size);
	//metaferoume thn "kefalh" ston deytero block
	long pos = lseek(fd, block_size, SEEK_SET);
	//grafoume to block bit map pou dhmiourghsame/arxikopoihsame
	write(fd, bbm, blocks_needed_for_bbm*block_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou block bit map pou grapsame
	long pos2 = lseek(fd, pos+(blocks_needed_for_bbm*block_size), SEEK_SET);
	//grafoume to i-nodes bit map pou dhmiourghsame/arxikopoihsame
	write(fd, ibm, blocks_needed_for_ibm*block_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou i-nodes bit map pou grapsame
	long pos3 = lseek(fd, pos2+(blocks_needed_for_ibm*block_size), SEEK_SET);
	//grafoume to directory table pou dhmiourghsame/arxikopoihsame
	write(fd, dt, blocks_needed_for_dt*block_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou directory table pou grapsame
	lseek(fd, pos3+(blocks_needed_for_dt*block_size), SEEK_SET);
	//telos grafoume ta i-nodes pou dhmiourghsame/arxikopoihsame
	write(fd, ind, blocks_needed_for_ind*block_size);

	printf("File system created and initialized successfuly!\n");

	//eleu8eronoume thn mnhmh pou desmeusame gia to superbloc
	free(sb);

	//"kleinoume" ton "disko"
	close(fd);

	//8etoume thn metablhth mounted se false
	mounted = FALSE;
}

/*
 * H synarthsh-utility mount kanei ola ta aparaithta allocations sthn kuria
 * mnhmh kai sth synexeia diabazei apo ton "disko" sthn kuria mnhmh tis
 * basikes domes pou elegxoun th leitourgia tou systhmatos arxeion.
 */
void mount(void) {

	//an to file system exei ginei hdh mounted typose analogo mhnyma kai epestrepse
	if(mounted) {
		printf("File system is already mounted!\n");
		return;
	}

	//"anoigma" tou diskou mono gia anagnosh
	int fd = open(NAME_OF_VIRTUAL_DISK, O_RDONLY);
	//an epistrafhke -1 typose analogo mhnyma
	if(fd == -1) {
		printf("Error: cannot access disk!\n");
		return;
	}

    //desmeusi enos char buffer me megethos sizeof(superblock) bytes
    char *buffer = malloc(sizeof(superblock));
    //midenismos tou buffer
    bzero(buffer, sizeof(superblock));

	//metafora ths "kefalhs" tou "diskou" sthn arxh tou
	lseek(fd, 0L, SEEK_SET);
	//prospa8hse na diabaseis to superblock kai apo8hkeuse to ston buffer
	int n = read(fd, buffer, sizeof(superblock));
	//an den diabasthke tipota(0 bytes) shmainei pos to systhma arxeion den exei dhmiourgh8ei/arxikopoih8ei
	//opote typose analoga mhnyma kai epestrepse.
	if(n == 0) {
		printf("Command cannot execute because file system isn't initialized yet.\n");
		printf("Initialize the file system with the 'mkfs' command first!\n");
		return;
	}

	printf("Mounting the file system...\n");

	//desmeush mnhmhs gia to superblock
	the_superblock = (superblock*)malloc(sizeof(superblock));
	//antigrafh ton periexomenon tou buffer sthn global metablhth superblock
	the_superblock = (superblock*)buffer;

	//apo8hkeuoume(stis katallhles metablhtes) kapoia aparaithta mege8h(se ari8mo blocks) pou xreiazontai gia thn anagnosh ton domon apo ton "disko"
	int block_size = the_superblock->block_size;
	int bbm_blocks = the_superblock->used_blocks_for_block_bmap;
	int bbm_size = bbm_blocks * block_size;
	int ibm_blocks = the_superblock->used_blocks_for_inodes_bmap;
	int ibm_size = ibm_blocks * block_size;
	int dt_blocks = the_superblock->used_blocks_for_dir_table;
	int dt_size = dt_blocks * block_size;
	int ind_blocks = the_superblock->used_blocks_for_inodes;
	int ind_size = ind_blocks * block_size;

    //desmeusi enos char buffer me megethos bbm_size bytes
    buffer = malloc(bbm_size);
    //midenismos tou buffer
    bzero(buffer, bbm_size);
	//metaferoume thn kefalh tou buffer sto deytero block tou diskou me skopo na diabasoume to block bit map
	long pos = lseek(fd, block_size, SEEK_SET);
	//diabasma tou block bit map kai apo8hkeush ston buffer
	read(fd, buffer, bbm_size);

	//desmeush mnhmhs gia to block bit map
	block_bit_map = (unsigned char*)malloc(the_superblock->bbmap_elements * sizeof(char));
	//antigrafh ton periexomenon tou buffer sthn global metablhth block_bit_map
	block_bit_map = (unsigned char*)buffer;

    //desmeusi enos char buffer me megethos ibm_size bytes
    buffer = malloc(ibm_size);
    //midenismos tou buffer
    bzero(buffer, ibm_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou block bit map me
	//skopo na diabasoume to i-nodes bit map
	long pos2 = lseek(fd, pos+bbm_size, SEEK_SET);
	//diabasma tou i-nodes bit map kai apo8hkeush ston buffer
	read(fd, buffer, ibm_size);

	//desmeush mnhmhs gia to i-nodes bit map
	inodes_bit_map = (unsigned char*)malloc(the_superblock->ibmap_elements * sizeof(char));
	//antigrafh ton periexomenon tou buffer sthn global metablhth inodes_bit_map
	inodes_bit_map = (unsigned char*)buffer;

    //desmeusi enos char buffer me megethos dt_size bytes
    buffer = malloc(dt_size);
    //midenismos tou buffer
    bzero(buffer, dt_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou i-nodes bit map me
	//skopo na diabasoume to directory table
	long pos3 = lseek(fd, pos2+ibm_size, SEEK_SET);
	//diabasma tou directory table kai apo8hkeush ston buffer
	read(fd, buffer, dt_size);

	//desmeush mnhmhs gia to directory table
	dir_table = (directory_table*)malloc(the_superblock->num_of_inodes * sizeof(directory_table));
	//antigrafh ton periexomenon tou buffer sthn global metablhth dir_table
	dir_table = (directory_table*)buffer;

    //desmeusi enos char buffer me megethos ind_size bytes
    buffer = malloc(ind_size);
    //midenismos tou buffer
    bzero(buffer, ind_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou directory table me
	//skopo na diabasoume ta i-nodes
	lseek(fd, pos3+dt_size, 0);
	//diabasma ton i-nodes kai apo8hkeush ston buffer
	read(fd, buffer, ind_size);

	//desmeush mnhmhs gia ta i-nodes
	inodes = (i_node*)malloc(the_superblock->num_of_inodes * sizeof(i_node));
	//antigrafh ton periexomenon tou buffer sthn global metablhth inodes
	inodes = (i_node*)buffer;

	//"kleinoume" ton "disko"
	close(fd);

	//8etoume thn metablhth mounted se true
	mounted = TRUE;
	//8etoume thn metablhth unmounted se false
	unmounted = FALSE;

	printf("File system mounted successfuly!\n");
}

/*
 * H synarthsh-utility umount grafei tis domes elegxou tou file system mas
 * apo th mnhmh ston "disko", kleinei ta anoixta arxeia kai apodesmeuei th
 * mnhmh pou exei desmeytei apo to systhma arxeion.
 */
void umount(void) {
	//"anoigma" tou "diskou mono gia eggrafh"
	int fd = open(NAME_OF_VIRTUAL_DISK, O_WRONLY);
	//an epistrafhke -1 typose analogo mhnyma
	if(fd == -1) {
		printf("Error: cannot access disk!\n");
		return;
	}

	printf("Unmounting the file system...\n");

	//apo8hkeuoume(stis katallhles metablhtes) kapoia aparaithta mege8h(se ari8mo blocks) pou xreiazontai gia thn eggrafh ton domon ston "disko"
	int block_size = the_superblock->block_size;
	int bbm_size = the_superblock->used_blocks_for_block_bmap * block_size;
	int ibm_size = the_superblock->used_blocks_for_inodes_bmap * block_size;
	int dt_size = the_superblock->used_blocks_for_dir_table * block_size;
	int ind_size = the_superblock->used_blocks_for_inodes * block_size;

	//metaferoume thn "kefalh" diskou sthn arxh tou
	lseek(fd, 0L, SEEK_SET);
	//grafoume to suberblock to opoio katalambanei ena block
	write(fd, the_superblock, block_size);
	//metaferoume thn "kefalh" ston deytero block
	long pos = lseek(fd, block_size, SEEK_SET);
	//grafoume to block bit map
	write(fd, block_bit_map, bbm_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou block bit map
	long pos2 = lseek(fd, pos+bbm_size, SEEK_SET);
	//grafoume to i-nodes bit map
	write(fd, inodes_bit_map, ibm_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou i-nodes bit map
	long pos3 = lseek(fd, pos2+ibm_size, SEEK_SET);
	//grafoume to directory table
	write(fd, dir_table, dt_size);
	//metaferoume thn "kefalh" sthn arxh tou epomenou block apo to teleutaio block tou directory table
	lseek(fd, pos3+dt_size, SEEK_SET);
	//telos grafoume ta i-nodes
	write(fd, inodes, ind_size);

	//eley8eronoume thn mnhmh pou eixame desmeusei sthn mount gia tis domes tou file system
	free(the_superblock);
	free(block_bit_map);
	free(inodes_bit_map);
	free(dir_table);
	free(inodes);

	//"kleinoume" ton "disko"
	close(fd);

	//8etoume thn metablhth unmounted se true
	unmounted = TRUE;
	//8etoume thn metablhth mounted se false
	mounted = FALSE;

	printf("File system unmounted successfuly!\n");
}

/*
 * H synarthsh-utility echo exei duo leitourgies. Me thn leitourgia '>' dhmiourgei
 * ena neo arxeio file eno me thn leitourgia '>>' synenonei to periexomeno tou arxeiou
 * file(an yparxei) me ayto pou edose o xrhsths(kanei append to keimeno tou xrhsth
 * sto arxeio), to opoio arxeio file dexetai os orisma. Pairnei dyo orismata, to proto
 * einai o ari8mos ton parametron pou exei dosei o xrhsths kai to deytero einai enas
 * pinakas me deiktes se chars pou periexei tis parametrous(onoma arxeiou pros ektyposh)
 * pou edose o xrhsths. PROSOXH: Na shmeio8ei pos gia na leitourghsei sosta h echo (etsi
 * opos exei ylopoih8ei) prepei afou o xrhsths teleiosei me thn eisagogh tou keimenou na
 * pathsei mia fora prota enter kai meta na path8ei o synduasmos Ctrl-D pou shmatodotei
 * to telos tou arxeiou.
 */
void echo(int argc, char *argv[]) {
	//an o ari8mos ton parametron einai diaforos tou 2 typose analogo mhnyma kai epestrepse
	if(argc != 2) {
		printf("Invalid number of parameters!\n");
		return;
	}

	//an oi parametroi einai 2 kai h deyterh parametros pou einai to onoma tou arxeiou exei mhkos
	//megalutero tou megistou epitreptou, typose analogo mhnyma kai epestrepse
	if(strlen(argv[1]) > MAX_LENGTH_OF_FILENAME) {
		printf("You must enter a filename with maximum length of 16 characters.\n");
		return;
	}

	//enas buffer xorhtikothtas 10000 chars
	char buffer[10000];
	//mhdenismos tou buffer
	bzero(buffer, sizeof(buffer));

	//orizoume tis epitreptes leitourgeis ths echo
	const char *valid_operations[] = {">", ">>"};
	//metablhth gia to an h leitourgia einai egkyrh h mh
	int valid_operation = FALSE;

	//apo8hkeuoume tis parametrous stis metablhtes operatio kai file antistoixa
	char *operation = argv[0];
	char *file = argv[1];

	//elegxoume an h leitourgia pou edose o xrhsths os parametro einai egkyrh
	int i;
	for(i=0; i<3; i++) {
		if(strcmp(valid_operations[i], operation) == 0) {
			valid_operation = TRUE;	//egkyrh leitourgia
			break;
		}
	}

	//an h leitourgia den einai egkyrh typonoume analogo mhnyma kai epistrfoume
	if(!valid_operation) {
		printf("The operation you entered isn't valid!\n");
		return;
	}


	if(strcmp(valid_operations[0], operation) == 0) {	//an h leitourgia pou edose o xrhsths einai h '>'
		//dexomaste to keimeno pou eisagei o xrhsths kai to apo8hkeuoume ston buffer
		while(fgets(buffer, sizeof(buffer), stdin)) {
		}

		//prospa8hse na dhmiourghseis to arxeio me onoma file
		int success = create(file);

		if(success == 0) {	//an h dhmiourgia tou arxeiou htan epitixhs
			//prospa8hse na anoikseis to arxeio file
			int ufid = my_open(file);
			if(ufid >= 0) {	//an to arxeio anoikse epituxos
				//antigrafh tou buffer sto arxeio kai apo8hkeush ton bytes pou grafthkan sthn metablhth bytes_written
				int bytes_written = my_write(ufid, buffer, strlen(buffer), 0);
				//an epistrafhke to -1 shmainei pos o diskos exei gemise, opote typose analogo mhnyma
				if(bytes_written == -1)
					printf("The text wasn't saved because the disk is full!\n");
				else if((unsigned)bytes_written < strlen(buffer)-1) {	//allios an den grafthkan ola ta bytes, typose analoga mhnyma
					printf("The following text could not be written because there wasn't enough space in the disk");
					printf("(disk is full or indexed blocks of file are full of data):\n");
					for(i=bytes_written; (unsigned)i<strlen(buffer); i++)
						printf("%c", buffer[i]);
				}
				//kleisimo tou arxeiou
				my_close(ufid);
			}
		}
	}
	else if(strcmp(valid_operations[1], operation) == 0) {	//an h leitourgia pou edose o xrhsths einai h '>>'
		int ufid = my_open(file);
		if(ufid >= 0) {
			//dexomaste to keimeno pou eisagei o xrhsths kai to apo8hkeuoume ston buffer
			while(fgets(buffer, sizeof(buffer), stdin)) {
			}

			//antigrafh tou buffer sto telos tou arxeiou(append) kai apo8hkeush ton bytes pou grafthkan sthn metablhth bytes_written
			int bytes_written = my_write(ufid, buffer, strlen(buffer), file_size(ufid));
			//an epistrafhke to -1 shmainei pos o diskos exei gemise, opote typose analogo mhnyma
			if(bytes_written == -1)
				printf("The text wasn't saved because the disk is full!\n");
			else if((unsigned)bytes_written < strlen(buffer)-1) {	//allios an den grafthkan ola ta bytes, typose analoga mhnyma
					printf("The following text could not be written because there wasn't enough space in the disk");
					printf("(disk is full or indexed blocks of file are full of data):\n");
				for(i=bytes_written; (unsigned)i<strlen(buffer); i++)
					printf("%c", buffer[i]);
			}
			//kleisimo tou arxeiou
			my_close(ufid);
		}
		else	//an to arxeio den anoikse typose analogo mhnyma
			printf("echo: %s: No such file\n", file);
	}
}

/*
 * H synarthsh-utility cat ektyponei ta periexomena tou arxeiou file(an yparxei),
 * to opoio dexetai os orisma. Pairnei dyo orismata, to proto einai o ari8mos ton
 * parametron pou exei dosei o xrhsths kai to deytero einai enas pinakas me deiktes
 * se chars pou periexei tis parametrous(onoma arxeiou pros ektyposh) pou edose
 * o xrhsths.
 */
void cat(int argc, char *argv[]) {
	//an o ari8mos ton parametron einai diaforos tou 1 typose analogo mhnyma kai epestrepse
	if(argc != 1) {
		printf("Invalid number of parameters!\n");
		return;
	}

	//an exoume mia parametro, apo8hkeuse thn sthn metablhth file
	char *file = argv[0];

	//prospa8hse na anoikseis to arxeio file
	int ufid = my_open(file);
	if(ufid >= 0) {	//an to arxeio anoikse epituxos
		//dhlosh enos buffer me mege8os iso me to mege8os tou arxeiou+100(just in case)
		char buffer[file_size(ufid)+100];
		//midenismos tou buf
		bzero(buffer, sizeof(buffer));

		//diabasma tou arxeiou kai antigrafh ston buffer kai apo8hkeush ton bytes pou diabasthkan sth metablhth bytes_read
		int bytes_read = my_read(ufid, buffer, file_size(ufid), 0);
		//an dibasthkan bytes typose to arxeio
		if(bytes_read > 0)
			printf("%s\n", buffer);
		//kleise to arxeio
		my_close(ufid);
	}
	else	//an to arxeio den anoikse epituxos typose analogomhnyma
		printf("cat: %s: No such file\n", file);
}

/*
 * H synarthsh-utility ls ektyponei mia lista me ta arxeia tou systhmatos.
 */
void ls(void) {
	//orizoume enan pinaka me deiktes se char me mege8os iso me ton ari8mo ton
	//synolikon i-nodes, ara kai arxeion, pou exei to file system man.
	char *filenames[the_superblock->num_of_inodes];
	int i;
	//arxikopoioume tou deiktes se null
	for(i=0; i<the_superblock->num_of_inodes; i++)
		filenames[i] = NULL;

	//kaloume thn synarthsh files kai apo8hkeuoume to apotelesma sthn metablhth files_found
	int files_found = files(filenames);
	//an h metablhth files_found = 0, shmainei pos den bre8hke kanena arxeio kai epistrefoume
	if(files_found == 0)
		return;

	//allios typonoume ta arxeia pou bre8hkan
	for(i=0; i<files_found; i++)
		printf("%s ", filenames[i]);
	printf("\n");
}

/*
 * H synarthsh-utility cp antigrafei to arxeio source(an yparxei) sto arxeio dest,
 * ta opoia dexetai os orismata. Pairnei dyo orismata, to proto einai o ari8mos ton
 * parametron pou exei dosei o xrhsths kai to deytero einai enas pinakas me deiktes
 * se chars pou periexei tis parametrous(onomata arxeion pros antigrafh) pou edose
 * o xrhsths. Na shmeio8ei pos h leitourgia ths cp gia arxeio dest pou uparxei einai
 * to overwrite.
 */
void cp(int argc, char *argv[]) {
	//an o ari8mos ton parametron einai diaforos tou 2 typose analogo mhnyma kai epestrepse
	if(argc != 2) {
		printf("Invalid number of parameters!\n");
		return;
	}

	//an exoume dyo parametrous, apo8hkeuse tis stis metablhtes source kai dest antistoixa
	char *source = argv[0];
	char *dest = argv[1];

	//prospa8hse na anoikseis to arxeio source
	int ufid1 = my_open(source);

	if(ufid1 >= 0) {//an to arxeio source anoikse epituxos
		//dhlosh enos buffer me mege8os iso me to mege8os tou arxeiou
		char buffer[file_size(ufid1)+100];
		//midenismos tou buf
		bzero(buffer, sizeof(buffer));

		//diabasma olou tou arxeiou tou arxeiou kai antigrafh ston buffer
		my_read(ufid1, buffer, file_size(ufid1), 0);

		//prospa8hse na dhmiourghseis ena arxeio me onoma dest
		int result = create(dest);

		if(result == 0) {//an to arxeio dhmiourgh8hke epituxos
			//prospa8hse na anoikseis to arxeio dest
			int ufid2 = my_open(dest);
			if(ufid2 >= 0) {//an to arxeio anoikse epituxos
				//grapsimo olou tou buffer sto neo arxeio dest
				my_write(ufid2, buffer, strlen(buffer), 0);
				//kleisimo tou arxeiou dest
				my_close(ufid2);
			}
		}
		//kleisimo tou arxeiou source
		my_close(ufid1);
	}
	else	//an to arxeio source den anoikse typose analogo mhnyma
		printf("cp: cannot stat '%s': No such file\n", source);
}

/*
 * H synarthsh-utility mv metonomazei to arxeio source(an yparxei) se dest, ta opoia
 * dexetai os orismata. Pairnei dyo orismata, to proto einai o ari8mos ton parametron
 * pou exei dosei o xrhsths kai to deytero einai enas pinakas me deiktes se chars pou
 * periexei tis parametrous(onomata arxeion pros metonomasia) pou edose o xrhsths. Na
 * shmeio8ei pos h leitourgia ths mv gia arxeio dest pou uparxei einai to overwrite.
 */
void mv(int argc, char *argv[]) {
	//an o ari8mos ton parametron einai diaforos tou 2 typose analogo mhnyma kai epestrepse
	if(argc != 2) {
		printf("Invalid number of parameters!\n");
		return;
	}

	//an exoume dyo parametrous, apo8hkeuse tis stis metablhtes source kai dest antistoixa
	char *source = argv[0];
	char *dest = argv[1];

	//prospa8hse na anoikseis to arxeio source
	int ufid1 = my_open(source);

	if(ufid1 >= 0) {	//an to arxeio anoikse epituxos
		//prospa8hse na anoikseis to arxeio dest
		int ufid2 = my_open(dest);
		//an to arxeio dest yparxei, diegrapse to
		if(ufid2 >= 0) {
			delete(dest);
		}
		//metonomase to arxeio source se dest
		strcpy(dir_table[ufid1].filename, dest);
		//kleisimo tou arxeiou source
		my_close(ufid1);
	}
	else	//an to arxeio den anoikse typose analogo mhnyma
		printf("mv: cannot stat '%s': No such file\n", source);
}

/*
 * H synarthsh-utility rm diagrafei to arxeio(an yparxei) file, to opoio dexetai os orisma.
 * Pairnei dyo orismata, to proto einai o ari8mos ton parametron pou exei dosei
 * o xrhsths kai to deytero einai enas pinakas me deiktes se chars pou periexei
 * tis parametrous(onoma arxeiou pros diagrafh) pou edose o xrhsths.
 */
void rm(int argc, char *argv[]) {
	//an o ari8mos ton parametron einai diaforos tou 1 typose analogo mhnyma kai epestrepse
	if(argc != 1) {
		printf("Invalid number of parameters!\n");
		return;
	}

	//an exoume mia parametro, apo8hkeuse thn sthn metablhth file
	char *file = argv[0];

	//prospa8hse na digrapseis to arxeio me onoma file
	int result = delete(file);
	//an to arxeio den yparxei typose analogo mhnyma
	if(result != 0)
		printf("rm: cannot remove '%s': No such file\n", file);
}

/*
 * H synarthsh-utility df anaferei ta eley8era blocks tou systhmatos
 * kai ton synoliko ari8mo blocks.
 */
void df(void) {
	//ta synolika blocks tou systhmatos
	int total_blocks = the_superblock->total_blocks;
	//ta eleyuera blocks tou systhmatos
	int free_blocks = number_of_free_blocks();
	//ts xrhsimopoioumena blocks tou systhmatos
	int used_blocks = total_blocks - free_blocks;
	//xrhsh epi tois ekato ton xrhsimopoioumenon blocks
	float use = ((float)used_blocks / (float)total_blocks) * 100;

	printf("Total Blocks | Used Blocks | Free Blocks | Use%%\n");
	printf("  %10d |  %10d |  %10d | %d%%\n", total_blocks, used_blocks, free_blocks, (int)use);
}

/*
 * H synarthsh-utility help typonei mia lista me tis dia8esimes entoles.
 */
void help(void) {
	printf("Listing all available commands...\n");
	printf("\nusage: command [parameters]\n\n\n");
	printf("mkfs blocks block-size : makes and initializes the file system. \n");
	printf("            parameters : 'blocks' defines the number of blocks for the file system to\n");
	printf("                         be created and 'block_size' defines the size for each block.\n\n");
	printf("mount : mounts the file system.\n\n");
	printf("umount : umounts the file system.\n\n");
	printf("quit : quits the shell.\n\n");
	printf("help : listing all available commands(this command :)).\n\n");
	printf("ls : prints a list with all the files of the file system.\n\n");
	printf("cp source dest : copies the 'source' file(if exists) to the 'dest' file\n");
	printf("                 (overwriting any existing file).\n\n");
	printf("rm file : deletes the 'file'(if exists).\n\n");
	printf("df : prints a line with info about the block usage of the file system.\n\n");
	printf("cat file : prints the contents of 'file'(if exists).\n\n");
	printf("echo operation file : depending on the operation given either creates a 'file'\n");
	printf("                      or appends the texts to the end of an existing 'file'.\n");
}

/*
 * H synarthsh-utility quit prokalei termatismo tou shell.
 */
void quit(void) {
	//an to file system exei ginei mounted kai den exei ginei unmounted typose analogo mhnyma
	if(mounted && !unmounted)
		printf("You must unmount the file system with the 'umount' command before quiting!\n");
	else
		exit(0); //allios bges(termatise) apo to shell
}
