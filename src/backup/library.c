#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "fs_structs.h"

#define NAME_OF_VIRTUAL_DISK "virtual_disk.dat"

#define FALSE 0
#define TRUE 1
#define ONE_BYTE 8

/* ---------- BLOCK SERVICE --------- */
void allocate_block(int);

void deallocate_block(int);

void read_block(int, char *);

void write_block(int, char *);

void buffer_to_blocks(char *, char[][the_superblock->block_size]);

int find_free_blocks(char *, int);

int number_of_free_blocks(void);

int find_free_inode(void);

void bind_inode(int);

void clear_inode(int);
/* ---------------------------------- */

/* ---------- FILE SERVICE ---------- */
int my_close(int);

int file_size(int);

int my_read(int, char *, int, int);

int my_write(int, char *, int, int);
/* ---------------------------------- */

/* ------- DIRECTORY SERVICE -------- */
int files(char **);

int create(const char *);

int delete(const char *);

int my_open(const char *);
/* ---------------------------------- */


int files(char **filenames) {
	int num_of_files = 0;

	int i;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		if(strcmp(dir_table[i].filename, "") != 0)
			filenames[num_of_files++] = dir_table[i].filename;
	}

	return num_of_files;
}

int create(const char *filename) {
	printf("Inside lib's create function\n");

	int success = -1;
	int file_exists = FALSE;

	int first_available_inode = find_free_inode();
	bind_inode(first_available_inode);

	int i, index;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		if(strcmp(dir_table[i].filename, filename) == 0) {
			index = i;
			file_exists = TRUE;
			break;
		}
	}

	printf("first_availaible_inode_index = %d\n", first_available_inode);

	if(file_exists) {
		printf("File exists\n");
		// mhdenizoume ta periexomena tou arxeiou
		inodes[dir_table[index].inode_index].file_size = 0;
		int j;
		for(j=0; j<NUM_OF_INDEXED_BLOCKS; j++) {
			if(inodes[dir_table[index].inode_index].indexed_blocks[j] != -1) {
				deallocate_block(inodes[dir_table[index].inode_index].indexed_blocks[j]);
				inodes[dir_table[index].inode_index].indexed_blocks[j] = -1;
			}
		}
		success = 0;
	}
	else {
		printf("File doesn't exist\n");
		strcpy(dir_table[first_available_inode].filename, filename);
		dir_table[first_available_inode].inode_index = first_available_inode;
		inodes[dir_table[first_available_inode].inode_index].file_size = 0;
		success = 0;
	}

	/*----------------------- DEBUGGING ------------------------------- *
	for(i=0; i<the_superblock->bbmap_elements; i++)
		printf("block_bit_map[%d] = %d\n", i, block_bit_map[i]);

	printf("\n-------------------------------\n\n");

	for(i=0; i<the_superblock->ibmap_elements; i++)
		printf("inodes_bit_map[%d] = %d\n", i, inodes_bit_map[i]);
	* ---------------------------------------------------------------- */

	return success;
}

int delete(const char *filename) {
	printf("Inside lib's delete function\n");
	int success = -1;

	int i;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		if(strcmp(dir_table[i].filename, filename) == 0) {
			printf("DEBUG: dir_table[%d].filename = %s\n", i, dir_table[i].filename);
			int j, block_number;
			for(j=0; j<NUM_OF_INDEXED_BLOCKS; j++) {
				block_number = inodes[dir_table[i].inode_index].indexed_blocks[j];
				printf("DEBUG: block_number = %d\n", block_number);
				if(block_number != -1)
					deallocate_block(block_number);
			}
			strcpy(dir_table[i].filename, "");
			printf("DEBUG: dir_table[%d].inode_index = %d\n", i, dir_table[i].inode_index);
			clear_inode(dir_table[i].inode_index);
			dir_table[i].inode_index = -1;
			success = 0;
			break;
		}
	}

	if(success == 0) {
		printf("File deleted successfuly\n");
	}
	else {
		printf("File couldn't deleted!\n");
	}

	/*----------------------- DEBUGGING ------------------------------- *
	for(i=0; i<the_superblock->bbmap_elements; i++)
		printf("block_bit_map[%d] = %d\n", i, block_bit_map[i]);

	printf("\n-------------------------------\n\n");

	for(i=0; i<the_superblock->ibmap_elements; i++)
		printf("inodes_bit_map[%d] = %d\n", i, inodes_bit_map[i]);
	* ---------------------------------------------------------------- */

	return success;
}

int my_open(const char *filename) {
	printf("Inside lib's my_open function\n");
	int file_exists = FALSE;
	int file_id = -1;

	int i;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		if(strcmp(dir_table[i].filename, filename) == 0) {
			file_exists = TRUE;
			file_id = dir_table[i].inode_index;
			inodes[file_id].file_is_open = TRUE;
			break;
		}
	}

	if(file_exists) {
		printf("File exists\n");
	}
	else {
		printf("File doesn't exist\n");
	}

	return file_id;
}

int my_close(int ufid) {

	if(ufid >= 0 || ufid < the_superblock->num_of_inodes) {
		inodes[ufid].file_is_open = FALSE;
		return 0;
	}
	else
		return -1;
}

int file_size(int ufid) {
	return inodes[ufid].file_size;
}

int my_write(int ufid, char *buf, int num, int pos) {
	printf("Inside lib's my_write function\n");

	printf("pos = %d\n", pos);
	printf("num = %d\n", num);

	if(pos > file_size(ufid)) {
		printf("Error: position out of bounds!\n");
		return -1;
	}

	int bytes_to_be_written;
	if((unsigned)num == strlen(buf))
		bytes_to_be_written = num -1 ;	//den apo8hkeuoume ton xarakthra neas grammhs '\r' (num-1)
	else
		bytes_to_be_written = num;

	char buffer[strlen(buf)];
	bzero(buffer, sizeof(buffer));
	strncpy(buffer, buf, bytes_to_be_written);
	printf("strlen(buffer) (inside my_write) = %d\n", strlen(buffer));

	int bytes_written = 0;

	int blocks_needed = ceil((double)(bytes_to_be_written) / the_superblock->block_size);
	printf("blocks_needed = %d\n", blocks_needed);

	/* Briskoume (an yparxoun) blocks_needed to plh8os eleu8era blocks gia apo8hkeush tou arxeiou */
	char free_blocks[blocks_needed];

	int num_of_free_blocks = find_free_blocks(free_blocks, blocks_needed);

	printf("num_of_free_blocks = %d\n", num_of_free_blocks);

	if(num_of_free_blocks == 0) {
		return 0;
	}

	// spame to arxeio(string) se strings tou enos block kai ta
	// apo8hkeuoume ston pinaka string_blocks
	char string_blocks[blocks_needed][the_superblock->block_size];
	int i;
	for(i=0; i<blocks_needed; i++)
		memset(string_blocks[i], 0, sizeof(string_blocks[i]));

	//for(i=0; i<blocks_needed; i++)
	//	printf("%s\n", string_blocks[i]);

	printf("\n\n-------------------------------\n\n");

	//for(i=0; i<NUM_OF_INDEXED_BLOCKS; i++)
	//	printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);


	if(file_size(ufid) == 0) {
		buffer_to_blocks(buffer, string_blocks);

		if(num_of_free_blocks < blocks_needed) {
			for(i=0; i<num_of_free_blocks; i++) {
				inodes[ufid].indexed_blocks[i] = free_blocks[i];
				printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);
				// grafoume to arxeio(string) block by block
				write_block(inodes[ufid].indexed_blocks[i], string_blocks[i]);
			}
			bytes_written = num_of_free_blocks * the_superblock->block_size;
		}
		else if(num_of_free_blocks >= blocks_needed && blocks_needed > NUM_OF_INDEXED_BLOCKS) {
			for(i=0; i<NUM_OF_INDEXED_BLOCKS; i++) {
				inodes[ufid].indexed_blocks[i] = free_blocks[i];
				printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);
				// grafoume to arxeio(string) block by block
				write_block(inodes[ufid].indexed_blocks[i], string_blocks[i]);
			}
			bytes_written = NUM_OF_INDEXED_BLOCKS * the_superblock->block_size;
		}
		else if(num_of_free_blocks >= blocks_needed && blocks_needed <= NUM_OF_INDEXED_BLOCKS) {
			for(i=0; i<blocks_needed; i++) {
				inodes[ufid].indexed_blocks[i] = free_blocks[i];
				printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);
				// grafoume to arxeio(string) block by block
				write_block(inodes[ufid].indexed_blocks[i], string_blocks[i]);
			}
			bytes_written = bytes_to_be_written;
		}
		inodes[ufid].file_size = bytes_written;
	}
	else {
		int block_index = floor((double)pos / the_superblock->block_size);
		int pos_of_char = pos % the_superblock->block_size;

		char block[the_superblock->block_size];
		bzero(block, sizeof(block));
		printf("block_index = %d\n", block_index);
		printf("pos_of_char = %d\n", pos_of_char);

		int last_block_with_data = floor((double)inodes[ufid].file_size / the_superblock->block_size);
		int num_of_free_indexed_blocks;
		if(last_block_with_data == 0)
			num_of_free_indexed_blocks = 12;
		else
			num_of_free_indexed_blocks = NUM_OF_INDEXED_BLOCKS - last_block_with_data - 1;

		printf("last_block_with_data = %d\n", last_block_with_data);
		printf("num_of_free_indexed_blocks = %d\n", num_of_free_indexed_blocks);

		char temp_buf[blocks_needed*the_superblock->block_size];
		bzero(temp_buf, sizeof(temp_buf));

		if(block_index == last_block_with_data) {
			read_block(inodes[ufid].indexed_blocks[block_index-1], block);
			strcat(temp_buf, block);
		}
		else {
			int read_up_to;
			if(block_index+blocks_needed <= last_block_with_data)
				read_up_to = block_index+blocks_needed;
			else
				read_up_to = last_block_with_data;
			for(i=block_index; i<read_up_to; i++) {
				// diabazoume to arxeio(string) block by block
				read_block(inodes[ufid].indexed_blocks[i], block);
				strcat(temp_buf, block);
			}
		}

		//printf("temp_buf before\n");
		//printf("%s\n", temp_buf);
		//printf("\n\n-------------------------------\n\n");

		int j = 0;
		for(i=pos_of_char; i<pos_of_char+bytes_to_be_written; i++) {
			temp_buf[i] = buffer[j++];
		}

		//printf("temp_buf after\n");
		//printf("%s\n", temp_buf);
		//printf("\n\n-------------------------------\n\n");

		buffer_to_blocks(temp_buf, string_blocks);

		int k;
		if(num_of_free_blocks < blocks_needed) {
			printf("num_of_blocks_to_be_written = %d\n\n", num_of_free_blocks);
			if(block_index == last_block_with_data) {//append text
				write_block(inodes[ufid].indexed_blocks[block_index], string_blocks[0]);
				k = 0;
				j = 1;
				for(i=block_index+1; i<block_index+1+num_of_free_blocks; i++) {
					inodes[ufid].indexed_blocks[i] = free_blocks[k++];
					printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);
					// grafoume to arxeio(string) block by block
					write_block(inodes[ufid].indexed_blocks[i], string_blocks[j++]);
				}
				bytes_written = num_of_free_blocks * the_superblock->block_size + the_superblock->block_size - pos_of_char;
				inodes[ufid].file_size += bytes_written;
			}
			else {
				k = 0;
				j = 0;
				for(i=block_index; i<block_index+blocks_needed; i++) {
					printf("i == %d\n", i);
					if(i >= last_block_with_data+1) {
						printf("free_blocks[%d] = %d\n\n", k, free_blocks[k]);
						inodes[ufid].indexed_blocks[i] = free_blocks[k++];
						printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);
						write_block(inodes[ufid].indexed_blocks[i], string_blocks[j++]);
						num_of_free_blocks--;
						if(num_of_free_blocks==0)
							break;
					}
					else {
						// grafoume to buffer block by block
						write_block(inodes[ufid].indexed_blocks[i], string_blocks[j++]);
					}
				}
				if(num_of_free_blocks == 0) {
					bytes_written = (blocks_needed+num_of_free_blocks) * the_superblock->block_size + the_superblock->block_size - pos_of_char;
					inodes[ufid].file_size += bytes_written;
				}
				else {
					bytes_written = blocks_needed * the_superblock->block_size + the_superblock->block_size - pos_of_char;
				}
			}
		}
		else if(num_of_free_blocks >= blocks_needed && blocks_needed > num_of_free_indexed_blocks) {
			printf("num_of_blocks_to_be_written = %d\n\n", num_of_free_indexed_blocks);
			if(block_index == last_block_with_data) {//append text
				write_block(inodes[ufid].indexed_blocks[block_index], string_blocks[0]);
				k = 0;
				j = 1;
				for(i=block_index+1; i<NUM_OF_INDEXED_BLOCKS; i++) {
					inodes[ufid].indexed_blocks[i] = free_blocks[k++];
					printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);
					// grafoume to arxeio(string) block by block
					write_block(inodes[ufid].indexed_blocks[i], string_blocks[j++]);
				}
				bytes_written = num_of_free_indexed_blocks * the_superblock->block_size + the_superblock->block_size - pos_of_char;
				inodes[ufid].file_size += bytes_written;
			}
			else {
				k = 0;
				j = 0;
				for(i=block_index; i<block_index+blocks_needed; i++) {
					printf("i == %d\n", i);
					if(i >= last_block_with_data+1) {
						printf("free_blocks[%d] = %d\n\n", k, free_blocks[k]);
						inodes[ufid].indexed_blocks[i] = free_blocks[k++];
						printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);
						write_block(inodes[ufid].indexed_blocks[i], string_blocks[j++]);
						num_of_free_indexed_blocks--;
						if(num_of_free_indexed_blocks==0)
							break;
					}
					else {
						// grafoume to buffer block by block
						write_block(inodes[ufid].indexed_blocks[i], string_blocks[j++]);
					}
				}
				if(num_of_free_indexed_blocks == 0) {
					bytes_written = num_of_free_indexed_blocks * the_superblock->block_size + the_superblock->block_size - pos_of_char;
					inodes[ufid].file_size += bytes_written;
				}
				else {
					bytes_written = blocks_needed * the_superblock->block_size + the_superblock->block_size - pos_of_char;
				}
			}
		}
		else if(num_of_free_blocks >= blocks_needed && blocks_needed <= num_of_free_indexed_blocks) {
			printf("num_of_blocks_to_be_written = %d\n\n", blocks_needed);
			if(block_index == last_block_with_data) {
				write_block(inodes[ufid].indexed_blocks[block_index], string_blocks[0]);
				int k = 0;
				j = 1;
				for(i=block_index+1; i<block_index+blocks_needed; i++) {
					inodes[ufid].indexed_blocks[i] = free_blocks[k++];
					printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);
					// grafoume to arxeio(string) block by block
					write_block(inodes[ufid].indexed_blocks[i], string_blocks[j++]);
				}
				bytes_written = bytes_to_be_written;
				inodes[ufid].file_size += bytes_written;
			}
			else {
				j = 0;
				for(i=block_index; i<block_index+blocks_needed; i++) {
					// grafoume to buffer block by block
					write_block(inodes[ufid].indexed_blocks[i], string_blocks[j++]);
				}

				bytes_written = bytes_to_be_written;
			}
		}

	}

	printf("bytes_written = %d\n\n", bytes_written);

	//for(i=0; i<NUM_OF_INDEXED_BLOCKS; i++)
	//	printf("inodes[%d].indexed_blocks[%d] = %d\n", ufid, i, inodes[ufid].indexed_blocks[i]);

	printf("\n\n-------------------------------\n\n");

	/*----------------------- DEBUGGING ------------------------------- */
	//for(i=0; i<the_superblock->bbmap_elements; i++)
	//	printf("block_bit_map[%d] = %d\n", i, block_bit_map[i]);

	/*
	printf("\n-------------------------------\n\n");

	for(i=0; i<the_superblock->ibmap_elements; i++)
		printf("inodes_bit_map[%d] = %d\n", i, inodes_bit_map[i]);
	* ---------------------------------------------------------------- */

	return bytes_written;
}

void buffer_to_blocks(char *buf, char str_blocks[][the_superblock->block_size]) {
	int i, j, k;
	j = k = 0;
	int buf_len = strlen(buf);
	for(i=0; i<buf_len; i++) {
		str_blocks[j][k++] = buf[i];
		if(k == the_superblock->block_size) {
			j++;
			k = 0;
		}
	}
}

int my_read(int ufid, char *buf, int num, int pos) {
	printf("Inside lib's my_read function\n");

	//printf("pos = %d\n", pos);
	//printf("num = %d\n", num);

	if(pos > file_size(ufid)) {
		printf("Error: position out of bounds!\n");
		return -1;
	}

	int bytes_read = 0;

	int blocks_needed = ceil((double)(num) / the_superblock->block_size);
	//printf("blocks_needed = %d\n", blocks_needed);

	int block_index = floor((double)pos / the_superblock->block_size);
	int pos_of_char = pos % the_superblock->block_size;

	char block[the_superblock->block_size];
	bzero(block, sizeof(block));
	//printf("block_index = %d\n", block_index);
	//printf("pos_of_char = %d\n", pos_of_char);

	char temp_buf[blocks_needed*the_superblock->block_size];
	bzero(temp_buf, sizeof(temp_buf));

	int i;
	for(i=block_index; i<block_index+blocks_needed; i++) {
		// diabazoume to arxeio(string) block by block
		read_block(inodes[ufid].indexed_blocks[i], block);
		strcat(temp_buf, block);
	}

	int read_up_to;
	if(pos_of_char+num <= file_size(ufid))
		read_up_to = pos_of_char+num;
	else
		read_up_to = file_size(ufid);
	//printf("read_up_to = %d\n", read_up_to);
	int j = 0;
	for(i=pos_of_char; i<read_up_to; i++) {
		buf[j++] = temp_buf[i];
		bytes_read++;
	}

	printf("bytes_read = %d\n\n", bytes_read);

	return bytes_read;
}
