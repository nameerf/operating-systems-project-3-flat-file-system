/**********************************************************************************
 *                                                                                *
 * Sto arxeio ayto ylopoioume to directory service. Brisketai sto endiameso       *
 * epipedo tou file system mas, dhladh anamesa sto block service tou katotatou    *
 * epipedou kai sto anotero epipedo ton utilites. H basikh tou leitourgia einai h *
 * antistoixish ton onomaton ton arxeion me i-nodes. Yposthrizontai synarthseis   *
 * gia dhmiourgia kai diagrafh enos arxeiou, anoigma enos arxeiou kai ypolosimo   *
 * tou plh8ous ton arxeion tou systhmatos.                                        *
 *                                                                                *
 **********************************************************************************/

/* aparaithta includes */
#include <string.h>
#include <stdio.h>

#include "fs_structs.h"

//orismos ton 0 kai 1 os false kai true antistoixa
#define FALSE 0
#define TRUE 1

//dhloseis ton eksoterikon synarthseon tou block service ths biblio8hkhs
//etsi oste na mporesoume na tis xrhsimopoihsoume.
extern void deallocate_block(int);
extern int find_free_inode(void);
extern void bind_inode(int);
extern void clear_inode(int);

/* prototypa synarthseon */
int files(char **);
int create(const char *);
int delete(const char *);
int my_open(const char *);

/*
 * H synarthsh files epistrefei to plh8os ton yparxonton arxeion
 * pou periexei to systhma kai enan pinaka me ta onomata ton
 * yparxonton arxeion. Dexetai os orisma ena deikth se deikth se
 * char ston opoio apo8hkeyonta ta onomata.
 */
int files(char **filenames) {
	int num_of_files = 0;

	int i;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		if(strcmp(dir_table[i].filename, "") != 0)
			filenames[num_of_files++] = dir_table[i].filename;
	}

	return num_of_files;
}

/*
 * H synarths create dhmiourgei ena neo arxeio me onoma filename
 * kai mege8os 0. An yparxei arxeio me to idio onoma tote mhdenizei
 * to mege8os tou (dhladh diagrafontai ta palia tou periexomena).
 * Epistrefei 0 se epituxia kai -1 se apotyxia.
 */
int create(const char *filename) {
	int success = -1;
	int file_exists = FALSE;

	//saronoume to directory table apo thn arxh eos to telos kai
	int i, index;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		if(strcmp(dir_table[i].filename, filename) == 0) {//an broume kapoio onoma idio me to filename
			index = i;		//apo8hkeuoume ton index tou pinaka ton domon directory table
			file_exists = TRUE;		//to arxeio yparxei
			break;
		}
	}

	if(file_exists) {//an to arxeio yparxei mhdenizoume ta periexomena tou arxeiou
		inodes[dir_table[index].inode_index].file_size = 0;		//mhdenizoume to mege8os tou

		//gia ola ta deiktodotoumena blocks tou i-node
		int j;
		for(j=0; j<NUM_OF_INDEXED_BLOCKS; j++) {
			//ean deixnoun se kapoio block to 8etoume se -1 pou shmainei oti den deixnei pou8ena
			if(inodes[dir_table[index].inode_index].indexed_blocks[j] != -1) {
				//apodesmeuoume to block kai apo to block bit map
				deallocate_block(inodes[dir_table[index].inode_index].indexed_blocks[j]);
				//8etoume ton antistoixo index tou directory table se -i pou shmainei oti den antistoixizetai me kanena i-node
				inodes[dir_table[index].inode_index].indexed_blocks[j] = -1;
			}
		}
		success = 0;
	}
	else {//an to arxeio den yparxei to dhniourgoume
		//briskoume to proto dia8esimo i-node kai to apo8hkeuoume sth metablhth first_available_inode
		int first_available_inode = find_free_inode();
		//"desmeuoume" to i-node sto i-nodes bit map
		bind_inode(first_available_inode);
		//orizoume(antigrafoume) to filename os to onoma tou arxeioy
		strcpy(dir_table[first_available_inode].filename, filename);
		//antistoixizoume to i-node me ton antistoixo deikth tou directory table
		dir_table[first_available_inode].inode_index = first_available_inode;
		//8etoume to mege8os tou arxeiou se 0 bytes.
		inodes[dir_table[first_available_inode].inode_index].file_size = 0;
		success = 0;
	}

	return success;
}

/*
 * H synarthsh delete diagrafei to arxeio me onoma filename kai
 * epistrefei 0 se epityxia kai -1 se apotyxia (an to arxeio den bre8hke).
 */
int delete(const char *filename) {
	int success = -1;

	//saronoume to directory table apo thn arxh eos to telos kai
	int i;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		if(strcmp(dir_table[i].filename, filename) == 0) {//an broume kapoio onoma idio me to filename
			int j;
			//gia ola ta deiktodotoumena blocks tou i-node
			for(j=0; j<NUM_OF_INDEXED_BLOCKS; j++) {
				//ean deixnoun se kapoio block to 8etoume se -1 pou shmainei oti den deixnei pou8ena
				if(inodes[dir_table[i].inode_index].indexed_blocks[j] != -1)
					deallocate_block(inodes[dir_table[i].inode_index].indexed_blocks[j]);	//apodesmeuoume to block kai apo to block bit map
			}
			strcpy(dir_table[i].filename, "");		//8etoume to onoma tou arxeiou se keno
			clear_inode(dir_table[i].inode_index);	//ka8arizoume to i-node apo to i-nodes bit map
			//8etoume ton antistoixo index tou directory table se -i pou shmainei oti den antistoixizetai me kanena i-node
			dir_table[i].inode_index = -1;
			success = 0;	//epituxhs diagrafh
			break;
		}
	}

	return success;
}

/*
 * H synarthsh my_open(onomasthke etsi gia apofygh pi8anon conflicts
 * me thn synarthsh open tou unix) anoigei to arxeio me to onoma filename
 * (an yparxei) gia diabasma h/kai grapsimo kai epistrfei to ufid tou, to
 * opoio einai enas 8etikos ari8mos h mhden. An to arxeio den yparxei
 * epistrefetai -1.
 */
int my_open(const char *filename) {
	//to unique file id tou arxeiou
	int file_id = -1;

	//saronoume to directory table apo thn arxh eos to telos kai an
	//broume kapoio onoma idio me to filename apo8hkeyoyme to ufid,
	//8etoume thn metablhth file_is_open se TRUE pou shmainei pos
	//to arxeio einai anoixto kai stamatame.
	int i;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		if(strcmp(dir_table[i].filename, filename) == 0) {
			file_id = dir_table[i].inode_index;
			inodes[file_id].file_is_open = TRUE;
			break;
		}
	}

	return file_id;
}
