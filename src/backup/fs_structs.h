/**********************************************************************
 *                                                                    *
 * Sto arxeio ayto dhlonoume tis basikes domes tou systhmatos arxeion *
 * oi opoies einai: to superblock, ta block kai i-nodes bit maps, to  *
 * directory table kai telos ta i-nodes. Aytes xrhsimopoiountai toso  *
 * apo th biblio8hkh oso kai apo to shell.                            *
 *                                                                    *
 **********************************************************************/

#define MAX_LENGTH_OF_FILENAME 16	//orizoume to megisto mhkos pou mporei na exei ena arxeio
#define NUM_OF_INDEXED_BLOCKS 12	//orizoume to megisto pl8uos ton blocks pou deiktodotei ena i-node


/* dhlosh domhs typou superblock */
typedef struct {
    unsigned short int block_size;						//to mege8os tou block tou file system mas
	unsigned short int total_blocks;					//o olikos ari8mos ton blocks tou file system mas
	unsigned short int used_blocks_for_block_bmap;		//o ari8mos ton blocks pou xrhsimopoiountai gia to block bit map
	unsigned short int bbmap_elements;					//o ari8mos ton stoixeion tou pinaka block_bit_map
    unsigned short int used_blocks_for_inodes_bmap;		//o ari8mos ton blocks pou xrhsimopoiountai gia to i-nodes bit map
	unsigned short int ibmap_elements;					//o ari8mos ton stoixeion tou pinaka inodes_bit_map
    unsigned short int used_blocks_for_dir_table;		//o ari8mos ton blocks pou xrhsimopoiountai gia to directory table
	unsigned short int used_blocks_for_inodes;			//o ari8mos ton blocks pou xrhsimopoiountai gia ta i-nodes
	unsigned short int number_of_data_blocks;			//o ari8mos ton block dedomenon tou file system mas
	unsigned short int num_of_inodes;					//o ari8mos ton sunolikon i-nodes, ara kai arxeion, tou file system mas
	unsigned short int data_start_location;				//o ari8mos tou protou block dedomenon
} superblock;

/* dhlosh domhs typou directory_table */
typedef struct {
    char filename[MAX_LENGTH_OF_FILENAME];				//to filename tou ka8e arxeiou
    int inode_index;									//o ari8mos(deikths) tou i-node tou arxeiou
} directory_table;

/* dhlosh domhs typou i_node */
typedef struct {
    unsigned int file_size;								//to mege8os tou arxeiou
	short int file_is_open;								//metablhth pou ka8orizei an to arxeio einai anoixto h kleisto
    short int indexed_blocks[NUM_OF_INDEXED_BLOCKS];	//pinakas pou periexei deiktes sta blocks(tou arxeiou) pou deiktodotei ka8e i-node
} i_node;

/* edo dhlonoume tis metablhtes ton basikon domon tou file system mas */
superblock *the_superblock;
//to block bit map perilamnbanei MONO ta blocks ton dedomenon
unsigned char *block_bit_map;
unsigned char *inodes_bit_map;
directory_table *dir_table;
i_node *inodes;
