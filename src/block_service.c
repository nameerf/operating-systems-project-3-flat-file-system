/**********************************************************************************
 *                                                                                *
 * Sto arxeio ayto ylopoioume to block service. Brisketai sto xamhlotero          *
 * epipedo tou file system mas kai prosbash se ayto exoun mono ta services        *
 * tou anoterou epipedou dhladh ta file kai directory services. Yposthrizontai    *
 * basikes synarthseis gia desmeysh, apodesmeysh, anagnosh kai eggrafh eno block. *
 * Epishs parexontai kai kapoies epipleon boh8itikes synarthseis.                 *
 *                                                                                *
 **********************************************************************************/

/* aparaithta includes */
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "fs_structs.h"

//edo orizoume to to onoma tou "arxeiou tou systhmatos" (eikonikou diskou)
#define NAME_OF_VIRTUAL_DISK "virtual_disk.dat"

//ena byte == 8 bits :)
#define ONE_BYTE 8

/* prototypa synarthseon */
void write_block(int, char *);
void read_block(int, char *);
void allocate_block(int);
void deallocate_block(int);
int find_free_blocks(char *, int);
int number_of_free_blocks(void);
void buffer_to_blocks(char *, char[][the_superblock->block_size]);
int find_free_inode(void);
void bind_inode(int);
void clear_inode(int);

/*
 * H synarthsh write_block einai ypey8ynh gia thn eggrafh enos block ston
 * eikoniko mas disko. Dexetai dyo orismata, ton ari8mo tou block kai
 * ta periexomena tou block. Den epistrefei tipota.
 */
void write_block(int block_number, char* buf) {
	/* Elegxoume an o ari8mos tou block pou do8hke einai mesa sta oria ton block dedomenon tou "diskou".
	 * An den einai (pragma pou shmainei pos yparxei kapoio bug sta file h directory services) typonoume
	 * analogo mhnyma. */
    if((block_number >= 0) && (block_number < the_superblock->number_of_data_blocks)) {

		//"anoigoume" ton "disko" mono gia eggrafh
        int fd = open(NAME_OF_VIRTUAL_DISK, O_WRONLY);

		//8etoume thn kefalh tou "diskou" sto antistoixo block pou do8hke os orima metrontas apo to proto block dedomenon
        lseek(fd, (the_superblock->data_start_location+block_number)*the_superblock->block_size, SEEK_SET);

		//grafoume to block sto dikso
        write(fd, buf, the_superblock->block_size);

		//"kleinoume" ton disko
        close(fd);

		//shmeionoume sto block bit map to block pou molis grafthke
        allocate_block(block_number);
    }
    else
        printf("write_block: out of range!\n");
}

/*
 * H synarthsh read_block einai ypey8ynh gia thn anagnosh enos block apo
 * ton eikoniko mas disko. Dexetai dyo orismata, ton ari8mo tou block kai
 * enan buffer ston opoio apo8hkeyontai ta periexomena tou block.Den
 * epistrefei tipota.
 */
void read_block(int block_number, char* buf) {
	/* Elegxoume an o ari8mos tou block pou do8hke einai mesa sta oria ton block dedomenon tou "diskou".
	 * An den einai (pragma pou shmainei pos yparxei kapoio bug sta file h directory services) typonoume
	 * analogo mhnyma. */
    if((block_number >= 0) && (block_number < the_superblock->number_of_data_blocks)) {

		//"anoigoume" ton "disko" mono gia anagnosh
        int fd = open(NAME_OF_VIRTUAL_DISK, O_RDONLY);

		//8etoume thn kefalh tou "diskou" sto antistoixo block pou do8hke os orima metrontas apo to proto block dedomenon
        lseek(fd, (the_superblock->data_start_location+block_number)*the_superblock->block_size, SEEK_SET);

		//diabazoume to block apo to dikso
        read(fd, buf, the_superblock->block_size);

		//"kleinoume" ton disko
        close(fd);
    }
    else
        printf("read_block: out of range!\n");
}

/*
 * H synarthsh allocate_block einai ypey8ynh gia thn desmeysh enos block
 * ston eikoniko mas disko. Dexetai ton ari8mo tou block pros desmeydh kai
 * den epistrefei tipota. Ousiastika h desmeysh ginetai grafontas 1 sto
 * antistoixo bit tou block bit map.
 */
void allocate_block(int block_number) {
	/* Elegxoume an o ari8mos tou block pou do8hke einai mesa sta oria ton block dedomenon tou "diskou".
	 * An den einai (pragma pou shmainei pos yparxei kapoio bug sta file h directory services) typonoume
	 * analogo mhnyma. */
    if((block_number >= 0) && (block_number < the_superblock->number_of_data_blocks)) {

        //maska me tin opoia desmeuoume sigkekrimeno bit sto block bit map
        unsigned char allocate_bit;

		//o deikths sthn antistoixh 8esh tou block bit map(pinaka) pou periexei to bit pros desmeysh
        unsigned char index = block_number / ONE_BYTE;

		//to bit pros desmeush
        unsigned char bit = block_number - (index * ONE_BYTE);

		//elegxos gia to poio bit tou "block_bit_map[index]" 8eloume na kanoume 1 oste na dhmiourgisoume
        //thn katallili maska allocate_bit.
		switch(bit) {
			case 0: allocate_bit = 1; break;      //allocate_bit=00000001
			case 1: allocate_bit = 2; break;      //allocate_bit=00000010
			case 2: allocate_bit = 4; break;      //allocate_bit=00000100
			case 3: allocate_bit = 8; break;      //allocate_bit=00001000
			case 4: allocate_bit = 16; break;     //allocate_bit=00010000
			case 5: allocate_bit = 32; break;     //allocate_bit=00100000
			case 6: allocate_bit = 64; break;     //allocate_bit=01000000
			case 7: allocate_bit = 128; break;    //allocate_bit=10000000
        }

		//efoson grapsame thn zhtoumenh timh sthn maska allocate_bit, thn efrmozoume sto block bit map
		//etsi oste to kainourgio bit map pou prokuptei na exei kai to zhtoumeno bit desmeymeno (logiko 1)
        block_bit_map[index] |= allocate_bit;
    }
    else
        printf("allocate_block: out of range!\n");
}

/*
 * H synarthsh allocate_block einai ypey8ynh gia thn apodesmeysh enos block
 * apo ton eikoniko mas disko. Dexetai ton ari8mo tou block pros apodesmeydh
 * kai den epistrefei tipota. Ousiastika h apodesmeysh ginetai grafontas 0 sto
 * antistoixo bit tou block bit map.
 */
void deallocate_block(int block_number) {
	/* Elegxoume an o ari8mos tou block pou do8hke einai mesa sta oria ton block dedomenon tou "diskou".
	 * An den einai (pragma pou shmainei pos yparxei kapoio bug sta file h directory services) typonoume
	 * analogo mhnyma */
    if((block_number >= 0) && (block_number < the_superblock->number_of_data_blocks)) {

		//maska me tin opoia apodesmeuoume sigkekrimeno bit sto block bit map
        unsigned char deallocate_bit;

		//o deikths sthn antistoixh 8esh tou block bit map(pinaka) pou periexei to bit pros apodesmeysh
        unsigned char index = block_number / ONE_BYTE;

		//to bit pros apodesmeush
        unsigned char bit = block_number - (index * ONE_BYTE);

		//elegxos gia to poio bit tou "block_bit_map[index]" 8eloume na kanoume 0 oste na dhmiourgisoume
        //thn katallili maska deallocate_bit.
		switch(bit) {
			case 0: deallocate_bit = 1; break;      //deallocate_bit=00000001
			case 1: deallocate_bit = 2; break;      //deallocate_bit=00000010
			case 2: deallocate_bit = 4; break;      //deallocate_bit=00000100
			case 3: deallocate_bit = 8; break;      //deallocate_bit=00001000
			case 4: deallocate_bit = 16; break;     //deallocate_bit=00010000
			case 5: deallocate_bit = 32; break;     //deallocate_bit=00100000
			case 6: deallocate_bit = 64; break;     //deallocate_bit=01000000
			case 7: deallocate_bit = 128; break;    //deallocate_bit=10000000
        }

		//efoson grapsame thn zhtoumenh timh sthn maska deallocate_bit, thn efrmozoume sto block bit map
		//etsi oste to kainourgio bit map pou prokuptei na exei kai to zhtoumeno bit apodesmeymeno (logiko 0)
        block_bit_map[index] &= ~deallocate_bit;
    }
    else
        printf("deallocate_block: out of range!\n");
}

/*
 * H synarthsh find_free_blocks briksei, ean yparxoun, number se ari8mo eley8era blocks
 * kai apo8hkeuei (tous deiktes se ayta) ta eley8era blocks ston pinaka pou dinetai os orisma,
 * free_blocks.Epistrefei ton ari8mo ton eley8eron block pou bre8hkan o opoios einai panta <= number.
 */
int find_free_blocks(char* free_blocks, int number) {
	int i, j;
	j = 0;
	//"saronoume" to block bit map apo thn arxh mexri to telos kai otan
	//broume kapoio eley8ero block to apo8hkeuoume (ton deikth se ayto) ston
	//pinaka free_blocks kai ayksanoume ton ari8mo ton blocks pou bre8hkan kata
	//ena (j). An broume osa eley8era blocks xreiazomaste (number) stamatame alios
	//synexizoume mexri na ftasoume sto telos tou bit map. O elegxos gia to an ena
	//block einai eleu8ero ginetai me thn bitmask &, p.x. (block_bit_map[i] & 4) == 0
	//shmainei pos to to block pou dexnei to 3o bit tou stoixeiou toy bit map pou
	//brisketai sth 8esh i tou pinaka block_bit_map einai eley8ero (exei timh 0).
	for(i=0; i<the_superblock->bbmap_elements; i++) {
		if((block_bit_map[i] & 1) == 0) {
			free_blocks[j++] = i*ONE_BYTE;
			if(j == number)
				break;
		}
		if((block_bit_map[i] & 2) == 0) {
			free_blocks[j++] = i*ONE_BYTE + 1;
			if(j == number)
				break;
		}
		if((block_bit_map[i] & 4) == 0) {
			free_blocks[j++] = i*ONE_BYTE + 2;
			if(j == number)
				break;
		}
		if((block_bit_map[i] & 8) == 0) {
			free_blocks[j++] = i*ONE_BYTE + 3;
			if(j == number)
				break;
		}
		if((block_bit_map[i] & 16) == 0) {
			free_blocks[j++] = i*ONE_BYTE + 4;
			if(j == number)
				break;
		}
		if((block_bit_map[i] & 32) == 0) {
			free_blocks[j++] = i*ONE_BYTE + 5;
			if(j == number)
				break;
		}
		if((block_bit_map[i] & 64) == 0) {
			free_blocks[j++] = i*ONE_BYTE + 6;
			if(j == number)
				break;
		}
		if((block_bit_map[i] & 128) == 0) {
			free_blocks[j++] = i*ONE_BYTE + 7;
			if(j == number)
				break;
		}
	}

	return j;
}

/*
 * H synarthsh find_free_blocks ypologizei ton sunoliko ari8mo eley8eron block
 * tou "diskou". Den dexetai kanena orisma kai epistrefei ta synolika eley8era block.
 */
int number_of_free_blocks(void) {
	//o ari8mos ton sunolika eley8eron block
    int number_of_free_blocks = 0;

	//edo kanoume oti akribos kai sthn synarthsh find_free_blocks me thn monh
	//diafora oti den apo8hkeuoume ta eley8era block pou briskoume para mono
	//ta metrame.
	int i;
	for(i=0; i<the_superblock->bbmap_elements; i++) {
		if((block_bit_map[i] & 1) == 0)
			number_of_free_blocks++;
		if((block_bit_map[i] & 2) == 0)
			number_of_free_blocks++;
		if((block_bit_map[i] & 4) == 0)
			number_of_free_blocks++;
		if((block_bit_map[i] & 8) == 0)
			number_of_free_blocks++;
		if((block_bit_map[i] & 16) == 0)
			number_of_free_blocks++;
		if((block_bit_map[i] & 32) == 0)
			number_of_free_blocks++;
		if((block_bit_map[i] & 64) == 0)
			number_of_free_blocks++;
		if((block_bit_map[i] & 128) == 0)
			number_of_free_blocks++;
	}

    return number_of_free_blocks;
}

/*
 * H synarthsh find_free_blocks dexetai dyo orismata, enan deikth se buffer kai enan
 * disdiastato pinaka me aoristo plh8os grammon kai mhkos ka8e grammhs iso me to mege8os
 * tou block tou "diskou". Ayto pou kanei einai na "temaxisei" ta buffer se blocks mege8ous
 * isou me to block tou diskou etsi oste na apo8hkeytoun eykolotera. Den epistrefei tipota.
 */
void buffer_to_blocks(char *buf, char str_blocks[][the_superblock->block_size]) {
	int i, j, k;
	j = k = 0;
	int buf_len = strlen(buf);
	for(i=0; i<buf_len; i++) {
		str_blocks[j][k++] = buf[i];
		if(k == the_superblock->block_size) {
			j++;
			k = 0;
		}
	}
}

/*
 * H synarthsh find_free_inode briksei, ean yparxei adesmeyto,to epomeno eley8ero
 * i-node (ton deikth se ayto). Den dexetai kanena orismakai epistrefei to epomeno
 * eley8ero i-node(ton deikth se ayto) pou brhke h -1 an den brhke.
 */
int find_free_inode(void) {
	//to epomeno eley8ero i-node
	int free_inode = -1;

	//kai edo h logikh einai idia me tis synarthseis find_free_blocks kai number_of_free_blocks
	//me th diafora oti "saronoume" to i-nodes bit map kai epishs molis broume to proto eley8ero
	//i-node stamatame kai o epistrfoume.
	int i;
	for(i=0; i<the_superblock->ibmap_elements; i++) {
		if((inodes_bit_map[i] & 1) == 0) {
			free_inode = i*ONE_BYTE;
			break;
		}
		if((inodes_bit_map[i] & 2) == 0) {
			free_inode = i*ONE_BYTE + 1;
			break;
		}
		if((inodes_bit_map[i] & 4) == 0) {
			free_inode = i*ONE_BYTE + 2;
			break;
		}
		if((inodes_bit_map[i] & 8) == 0) {
			free_inode = i*ONE_BYTE + 3;
			break;
		}
		if((inodes_bit_map[i] & 16) == 0) {
			free_inode = i*ONE_BYTE + 4;
			break;
		}
		if((inodes_bit_map[i] & 32) == 0) {
			free_inode = i*ONE_BYTE + 5;
			break;
		}
		if((inodes_bit_map[i] & 64) == 0) {
			free_inode = i*ONE_BYTE + 6;
			break;
		}
		if((inodes_bit_map[i] & 128) == 0) {
			free_inode = i*ONE_BYTE + 7;
			break;
		}
	}

	return free_inode;
}

/*
 * H synarthsh bind_inode einai paromoia me thn allocate_block me thn diafora oti
 * leitourgei gia i-nodes anti gia blocks. Einai ypey8ynh gia thn "desmeysh" enos i-node,
 * dhaldh me thn antistoixish tou i-node me ton antistoixo ari8mo tou directory table
 * etsi oste na deixnei se kapoio sygkekrimeno arxeio. Dexetai ton ari8mo tou i-node pros
 * "desmeydh" kai den epistrefei tipota. Ousiastika h desmeysh ginetai grafontas 1
 * sto antistoixo bit tou i-nodes bit map.
 */
void bind_inode(int inode_number) {
	/* Elegxoume an o ari8mos tou i-node pou do8hke einai mesa sta oria ton ton synolikon i-nodes.
	 * An den einai (pragma pou shmainei pos yparxei kapoio bug sta file h directory services) typonoume
	 * analogo mhnyma */
    if((inode_number >= 0) && (inode_number < the_superblock->ibmap_elements * ONE_BYTE)) {

        //maska me tin opoia desmeuoume sigkekrimeno bit sto i-nodes bit map
        unsigned char allocate_bit;

		//o deikths sthn antistoixh 8esh tou i-nodes bit map(pinaka) pou periexei to bit pros desmeysh
        unsigned char index = inode_number / ONE_BYTE;

		//to bit pros desmeush
        unsigned char bit = inode_number - (index * ONE_BYTE);

		//elegxos gia to poio bit tou "inodes_bit_map[index]" 8eloume na kanoume 1 oste na dhmiourgisoume
        //thn katallili maska allocate_bit.
		switch(bit) {
			case 0: allocate_bit = 1; break;      //allocate_bit=00000001
			case 1: allocate_bit = 2; break;      //allocate_bit=00000010
			case 2: allocate_bit = 4; break;      //allocate_bit=00000100
			case 3: allocate_bit = 8; break;      //allocate_bit=00001000
			case 4: allocate_bit = 16; break;     //allocate_bit=00010000
			case 5: allocate_bit = 32; break;     //allocate_bit=00100000
			case 6: allocate_bit = 64; break;     //allocate_bit=01000000
			case 7: allocate_bit = 128; break;    //allocate_bit=10000000
        }

		//efoson grapsame thn zhtoumenh timh sthn maska allocate_bit, thn efrmozoume sto i-nodes bit map
		//etsi oste to kainourgio i-nodes bit map pou prokuptei na exei kai to zhtoumeno bit desmeymeno (logiko 1)
        inodes_bit_map[index] |= allocate_bit;
    }
    else
        printf("bind_inode: out of range!\n");
}

/*
 * H synarthsh clear_inode einai paromoia me thn deallocate_block me thn diafora oti
 * leitourgei gia i-nodes anti gia blocks. Einai ypey8ynh gia thn "apodesmeysh" enos i-node,
 * dhaldh me to na kanei to i-node na mh deixnei pou8ena. Dexetai ton ari8mo tou i-node pros
 * "apodesmeydh" kai den epistrefei tipota. Ousiastika h apodesmeysh ginetai grafontas 0
 * sto antistoixo bit tou i-nodes bit map.
 */
void clear_inode(int inode_number) {
	/* Elegxoume an o ari8mos tou i-node pou do8hke einai mesa sta oria ton ton synolikon i-nodes.
	 * An den einai (pragma pou shmainei pos yparxei kapoio bug sta file h directory services) typonoume
	 * analogo mhnyma */
    if((inode_number >= 0) && (inode_number < the_superblock->ibmap_elements * ONE_BYTE)) {

        //maska me tin opoia apodesmeuoume sigkekrimeno bit sto i-nodes bit map
        unsigned char deallocate_bit;

		//o deikths sthn antistoixh 8esh tou i-nodes bit map(pinaka) pou periexei to bit pros apodesmeysh
        unsigned char index = inode_number / ONE_BYTE;

		//to bit pros apodesmeush
        unsigned char bit = inode_number - (index * ONE_BYTE);

		//elegxos gia to poio bit tou "inodes_bit_map[index]" 8eloume na kanoume 0 oste na dhmiourgisoume
        //thn katallili maska deallocate_bit.
		switch(bit) {
			case 0: deallocate_bit = 1; break;      //deallocate_bit=00000001
			case 1: deallocate_bit = 2; break;      //deallocate_bit=00000010
			case 2: deallocate_bit = 4; break;      //deallocate_bit=00000100
			case 3: deallocate_bit = 8; break;      //deallocate_bit=00001000
			case 4: deallocate_bit = 16; break;     //deallocate_bit=00010000
			case 5: deallocate_bit = 32; break;     //deallocate_bit=00100000
			case 6: deallocate_bit = 64; break;     //deallocate_bit=01000000
			case 7: deallocate_bit = 128; break;    //deallocate_bit=10000000
        }

		//efoson grapsame thn zhtoumenh timh sthn maska deallocate_bit, thn efrmozoume sto i-nodes bit map
		//etsi oste to kainourgio i-nodes map pou prokuptei na exei kai to zhtoumeno bit apodesmeymeno (logiko 0)
        inodes_bit_map[index] &= ~deallocate_bit;
    }
    else
        printf("clear_inode: out of range!\n");
}
