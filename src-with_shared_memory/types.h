typedef struct {
    unsigned short int num_of_inodes;
    unsigned short int block_size;
	unsigned short int used_blocks_for_block_bmap;
    unsigned short int used_blocks_for_inodes_bmap;
    unsigned short int used_blocks_for_dir_table;
	unsigned short int used_blocks_for_inodes;
	unsigned short int number_of_data_blocks;
} superblock;

typedef struct {
    char filename[16];
    unsigned int inode_index;
} directory_table;

typedef struct {
    unsigned int file_size;
    unsigned short int indexed_blocks[13];
} i_node;
