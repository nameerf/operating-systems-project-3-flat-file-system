#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "types.h"

#define FALSE 0
#define TRUE 1

//kathorismos tou path kai tou onomatos tou virtual disk mas
#define NAME_OF_VIRTUAL_DISK "virtual_disk.dat"

void reverse(char []);

void itoa(int, char []);

/* ---------- BLOCK SERVICE ---------- */
//unsigned char read_bitmap(int);

//void write_bitmap(int, unsigned char);

int check_for_allocation(int);

void allocate_block(int);

void disallocate_block(int);

void read_block(int);

void write_block(int, char *);

int find_free_block(void);

int number_of_free_blocks(void);

int get_ufid(int);

void clear_inode(int);
/* ----------------------------------- */

/* ---------- FILE SERVICE ---------- */
int get_free_ufid(void);

int get_the_ufid(int, int);

int go_to_next_ufid(int);

int close(int);

int file_size(int);

int read(int, char *, int, int);

int write(int, char *, int, int);
/* ---------------------------------- */

/* ---------- DIRECTORY SERVICE ---------- */
int files(void);

int create(const char *);

int delete(const char *);

int my_open(const char *);
/* --------------------------------------- */


superblock *the_superblock;
unsigned char *block_bit_map;
unsigned char *inodes_bit_map;
directory_table *dir_table;
i_node *inodes;

/* Set the basic structures of file system */
void set_superblock(superblock *sb) {
	//printf("Inside lib's set_superblock function\n");
	the_superblock = sb;
}

void set_block_bit_map(unsigned char *bbm) {
	//printf("Inside lib's set_block_bit_map function\n");
	block_bit_map = bbm;
}

void set_inodes_bit_map(unsigned char *ibm) {
	//printf("Inside lib's set_inodes_bit_map function\n");
	inodes_bit_map = ibm;
}

void set_dir_table(directory_table *dt) {
	//printf("Inside lib's set_dir_table function\n");
	dir_table = dt;
}

void set_inodes(i_node *ind) {
	//printf("Inside lib's set_inodes function\n");
	inodes = ind;
}

int create(const char *filename) {
	printf("Inside lib's create function\n");

	int success = -1;
	int file_exists = FALSE;
	int first_availaible_inode_index = -1;

	//printf("the_superblock->num_of_inodes = %d\n\n", the_superblock->num_of_inodes);
	//printf("block_bit_map[3] = %d\n\n", block_bit_map[3]);
	//printf("inodes_bit_map[3] = %d\n\n", inodes_bit_map[2]);

	int i;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		//printf("dir_table[%d].filename = %s\n", i, dir_table[i].filename);
		if(dir_table[i].inode_index == -1 && first_availaible_inode_index == -1)
			first_availaible_inode_index = i;
		if(strcmp(dir_table[i].filename, filename) == 0) {
			file_exists = TRUE;
			//strcpy(dir_table[i].filename, "hello Panos!");
			break;
		}
	}

	printf("first_availaible_inode_index = %d\n", first_availaible_inode_index);

	if(file_exists) {
		printf("File exists\n");
		// TODO mhdenise ta periexomena tou arxeiou
		success = 0;
	}
	else {
		printf("File doesn't exist\n");
		strcpy(dir_table[first_availaible_inode_index].filename, filename);
		dir_table[first_availaible_inode_index].inode_index = first_availaible_inode_index;
		inodes[dir_table[first_availaible_inode_index].inode_index].file_size = 0;
		// TODO mhdenise tous indexes se blocks tou arxeiou
		success = 0;
	}

	return success;
}

int my_open(const char *filename) {
	printf("Inside lib's my_open function\n");
	int file_exists = FALSE;
	int file_id = -1;

	int i;
	for(i=0; i<the_superblock->num_of_inodes; i++) {
		//printf("dir_table[%d].filename = %s\n", i, dir_table[i].filename);
		if(strcmp(dir_table[i].filename, filename) == 0) {
			file_exists = TRUE;
			file_id = dir_table[i].inode_index;
			//strcpy(dir_table[i].filename, "hello Panos!");
			break;
		}
	}

	if(file_exists) {
		printf("File exists\n");
		return file_id;
	}
	else {
		printf("File doesn't exist\n");
	}

	return file_id;
}

int close(int ufid) {}

int write(int ufid, char *buf, int num, int pos) {

	int length = strlen(buf);
	//printf("length = %d\n", length);
	int blocks_needed;
	if(length % the_superblock->block_size == 0)
		blocks_needed = length / the_superblock->block_size;
	else
		blocks_needed = length / the_superblock->block_size + 1;
	//printf("blocks_needed = %d\n", blocks_needed);
	//printf("the_superblock->block_size = %d\n", the_superblock->block_size);

	char string_blocks[blocks_needed][the_superblock->block_size];
	int i;
	for(i=0; i<blocks_needed; i++)
		memset(string_blocks[i], 0, sizeof(string_blocks[i]));

	int j = 0;
	int k = 0;
	for(i=0; i<length; i++) {
		string_blocks[j][k++] = buf[i];
		if(k == the_superblock->block_size-1) {
			string_blocks[j][k] = '\0';
			j++;
			k = 0;
		}
	}

	//for(i=0; i<blocks_needed; i++)
	//	printf("%s", string_blocks[i]);
}
