#include <stdio.h>

int main(int argc, char *argv[]) {

	//int i;
	//for(i=0; i<argc; i++)
	//	printf("argv[%d] = %s\n", i, argv[i]);

	printf("Listing all available commands...\n");
	printf("command [optional parameters]\n\n");
	printf("mkfs blocks block-size\n");
	printf("mount\n");
	printf("umount\n");
	printf("quit\n");
	printf("help\n");
	printf("ls\n");
	printf("cp source dest\n");
	printf("rm file\n");
	printf("df\n");
	printf("cat file\n");
	printf("echo operation file\n");

	return 0;
}
