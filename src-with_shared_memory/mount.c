#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>		/* gia thn koinh mnhmh. */
#include <sys/shm.h>		/* gia thn koinh mnhmh. */

#include "types.h"

#define NAME_OF_VIRTUAL_DISK "virtual_disk.dat"

#define SHM_KEY_1 26151		/* to "kleidi 1" ths koinhs mnhmhs. */
#define SHM_KEY_2 26152		/* to "kleidi 2" ths koinhs mnhmhs. */
#define SHM_KEY_3 26153		/* to "kleidi 3" ths koinhs mnhmhs. */
#define SHM_KEY_4 26154		/* to "kleidi 4" ths koinhs mnhmhs. */
#define SHM_KEY_5 26155		/* to "kleidi 4" ths koinhs mnhmhs. */
#define SHM_SIZE_1 128		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */
#define SHM_SIZE_2 1024		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */
#define SHM_SIZE_3 8192		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */

int main(int argc, char *argv[]) {

	superblock *sb;
	unsigned char *bbm;
	unsigned char *ibm;
	directory_table *dt;
	i_node *ind;

    int shmid1, shmid2, shmid3, shmid4, shmid5, error1, error2, error3, error4, error5;
	superblock *sm_data1;
	unsigned char *sm_data2;
	unsigned char *sm_data3;
	directory_table *sm_data4;
	i_node *sm_data5;

	/* Locate the segment. */
    if ((shmid1 = shmget(SHM_KEY_1, SHM_SIZE_1, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data1 = shmat(shmid1, NULL, 0)) == (superblock *)-1) {
        perror("shmat");
        exit(1);
    }

	/* Locate the segment. */
    if ((shmid2 = shmget(SHM_KEY_2, SHM_SIZE_2, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data2 = shmat(shmid2, NULL, 0)) == (unsigned char *)-1) {
        perror("shmat");
        exit(1);
    }

	/* Locate the segment. */
    if ((shmid3 = shmget(SHM_KEY_3, SHM_SIZE_2, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data3 = shmat(shmid3, NULL, 0)) == (unsigned char *)-1) {
        perror("shmat");
        exit(1);
    }

	/* Locate the segment. */
    if ((shmid4 = shmget(SHM_KEY_4, SHM_SIZE_3, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data4 = shmat(shmid4, NULL, 0)) == (directory_table *)-1) {
        perror("shmat");
        exit(1);
    }

	/* Locate the segment. */
    if ((shmid5 = shmget(SHM_KEY_5, SHM_SIZE_3, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data5 = shmat(shmid5, NULL, 0)) == (i_node *)-1) {
        perror("shmat");
        exit(1);
    }

	printf("Mounting disk...\n");

	int fd = open(NAME_OF_VIRTUAL_DISK, O_RDONLY);

    //desmeusi enos char buf me megethos sizeof(superblock) bytes
    char *buffer = malloc(sizeof(superblock));
    //midenismos tou buf
    bzero(buffer, sizeof(superblock));

	lseek(fd, 0L, 0);
	read(fd, buffer, sizeof(superblock));

	sb = (superblock*)malloc(sizeof(superblock));
	sb = (superblock*)buffer;

	/*
	printf("superblock size is %d\n", sizeof(*the_superblock));
	printf("the_superblock->num_of_inodes = %d\n", the_superblock->num_of_inodes);
	printf("the_superblock->block_size = %d\n", the_superblock->block_size);
	printf("the_superblock->used_blocks_for_block_bmap = %d\n", the_superblock->used_blocks_for_block_bmap);
	printf("the_superblock->used_blocks_for_inodes_bmap = %d\n", the_superblock->used_blocks_for_inodes_bmap);
	printf("the_superblock->used_blocks_for_dir_table = %d\n", the_superblock->used_blocks_for_dir_table);
	printf("the_superblock->used_blocks_for_inodes = %d\n", the_superblock->used_blocks_for_inodes);
	printf("the_superblock->number_of_data_blocks = %d\n", the_superblock->number_of_data_blocks);
	*/

	int block_size = sb->block_size;
	int bbm_blocks = sb->used_blocks_for_block_bmap;
	int bbm_size = bbm_blocks * block_size;
	int ibm_blocks = sb->used_blocks_for_inodes_bmap;
	int ibm_size = ibm_blocks * block_size;
	int dt_blocks = sb->used_blocks_for_dir_table;
	int dt_size = dt_blocks * block_size;
	int ind_blocks = sb->used_blocks_for_inodes;
	int ind_size = ind_blocks * block_size;

    //desmeusi enos char buf me megethos bbm_size bytes
    buffer = malloc(bbm_size);
    //midenismos tou buf
    bzero(buffer, bbm_size);
	long pos = lseek(fd, sizeof(superblock), 0);
	read(fd, buffer, bbm_size);

	bbm = (unsigned char*)malloc(sizeof(char));
	bbm = (unsigned char*)buffer;
	int bbm_elements = sb->number_of_data_blocks/8;

	int i;
	//for(i=0; i<bbm_elements; i++)
	//	printf("block_bit_map[%d] = %d\n", i, block_bit_map[i]);

	//printf("\n-------------------------------\n\n");

    //desmeusi enos char buf me megethos ibm_size bytes
    buffer = malloc(ibm_size);
    //midenismos tou buf
    bzero(buffer, ibm_size);
	long pos2 = lseek(fd, pos+bbm_size, 0);
	read(fd, buffer, ibm_size);

	ibm = (unsigned char*)malloc(sizeof(char));
	ibm = (unsigned char*)buffer;
	int ibm_elements = sb->number_of_data_blocks/8;

	//for(i=0; i<ibm_elements; i++)
	//	printf("inodes_bit_map[%d] = %d\n", i, inodes_bit_map[i]);

	//printf("\n-------------------------------\n\n");

    //desmeusi enos char buf me megethos dt_size bytes
    buffer = malloc(dt_size);
    //midenismos tou buf
    bzero(buffer, dt_size);
	long pos3 = lseek(fd, pos2+ibm_size, 0);
	read(fd, buffer, dt_size);

	dt = (directory_table*)malloc(sizeof(directory_table));
	dt = (directory_table*)buffer;
	int dt_elements = sb->number_of_data_blocks;

	//int i;
	//for(i=0; i<dt_elements; i++) {
	//	printf("dir_table[%d].filename = %s\n", i, dt[i].filename);
	//	printf("dir_table[%d].inode_index = %d\n", i, dt[i].inode_index);
	//}

	//printf("\n-------------------------------\n\n\n");

    //desmeusi enos char buf me megethos ind_size bytes
    buffer = malloc(ind_size);
    //midenismos tou buf
    bzero(buffer, ind_size);
	lseek(fd, pos3+dt_size, 0);
	read(fd, buffer, ind_size);

	ind = (i_node*)malloc(sizeof(i_node));
	ind = (i_node*)buffer;
	int ind_elements = sb->number_of_data_blocks;

	//for(i=0; i<ind_elements; i++) {
	//	printf("inodes[%d].file_size = %d\n", i, inodes[i].file_size);
	//	printf("inodes[%d].indexed_blocks[%d] = %u\n", i, i, inodes[i].indexed_blocks[i]);
	//}

	close(fd);

	printf("Disk mounted successfuly!\n");

	*sm_data1 = *sb;

	for(i=0; i<bbm_elements; i++)
		sm_data2[i] = bbm[i];

	for(i=0; i<ibm_elements; i++)
		sm_data3[i] = ibm[i];

	for(i=0; i<dt_elements; i++)
		sm_data4[i] = dt[i];

	for(i=0; i<ind_elements; i++)
		sm_data5[i] = ind[i];

/*
	printf("\n\n-------------------------------\n\n");

	printf("the_superblock->num_of_inodes = %d\n\n", sm_data1->num_of_inodes);


	for(i=0; i<bbm_elements; i++)
		printf("block_bit_map[%d] = %d\n", i, sm_data2[i]);

	printf("\n-------------------------------\n\n");

	for(i=0; i<ibm_elements; i++)
		printf("inodes_bit_map[%d] = %d\n", i, sm_data3[i]);

	printf("\n-------------------------------\n\n");


	for(i=0; i<dt_elements; i++) {
		printf("dir_table[%d].filename = %s\n", i, sm_data4[i].filename);
		printf("dir_table[%d].inode_index = %d\n", i, sm_data4[i].inode_index);
	}

	printf("\n-------------------------------\n\n");

	for(i=0; i<ind_elements; i++) {
		printf("inodes[%d].file_size = %d\n", i, sm_data5[i].file_size);
		printf("inodes[%d].indexed_blocks[%d] = %u\n", i, i, sm_data5[i].indexed_blocks[7]);
	}
*/

	free(sb);
	free(bbm);
	free(ibm);

	error1 = shmdt(sm_data1); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error1 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	error2 = shmdt(sm_data2); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error2 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	error3 = shmdt(sm_data3); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error3 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	error4 = shmdt(sm_data4); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error4 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	error5 = shmdt(sm_data5); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error5 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	return 0;
}