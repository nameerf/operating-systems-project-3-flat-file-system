#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>		/* gia thn koinh mnhmh. */
#include <sys/shm.h>		/* gia thn koinh mnhmh. */

#include "types.h"

#define NAME_OF_VIRTUAL_DISK "virtual_disk.dat"

#define SHM_KEY_1 26151		/* to "kleidi 1" ths koinhs mnhmhs. */
#define SHM_KEY_2 26152		/* to "kleidi 2" ths koinhs mnhmhs. */
#define SHM_KEY_3 26153		/* to "kleidi 3" ths koinhs mnhmhs. */
#define SHM_KEY_4 26154		/* to "kleidi 4" ths koinhs mnhmhs. */
#define SHM_KEY_5 26155		/* to "kleidi 4" ths koinhs mnhmhs. */
#define SHM_SIZE_1 128		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */
#define SHM_SIZE_2 1024		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */
#define SHM_SIZE_3 8192		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */

int main(int argc, char *argv[]) {

    int shmid1, shmid2, shmid3, shmid4, shmid5, error1, error2, error3, error4, error5;
	superblock *sm_data1;
	unsigned char *sm_data2;
	unsigned char *sm_data3;
	directory_table *sm_data4;
	i_node *sm_data5;

	/* Locate the segment. */
    if ((shmid1 = shmget(SHM_KEY_1, SHM_SIZE_1, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data1 = shmat(shmid1, NULL, 0)) == (superblock *)-1) {
        perror("shmat");
        exit(1);
    }

	/* Locate the segment. */
    if ((shmid2 = shmget(SHM_KEY_2, SHM_SIZE_2, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data2 = shmat(shmid2, NULL, 0)) == (unsigned char *)-1) {
        perror("shmat");
        exit(1);
    }

	/* Locate the segment. */
    if ((shmid3 = shmget(SHM_KEY_3, SHM_SIZE_2, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data3 = shmat(shmid3, NULL, 0)) == (unsigned char *)-1) {
        perror("shmat");
        exit(1);
    }

	/* Locate the segment. */
    if ((shmid4 = shmget(SHM_KEY_4, SHM_SIZE_3, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data4 = shmat(shmid4, NULL, 0)) == (directory_table *)-1) {
        perror("shmat");
        exit(1);
    }

	/* Locate the segment. */
    if ((shmid5 = shmget(SHM_KEY_5, SHM_SIZE_3, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

	/* Now we attach the segment to our data space. */
    if ((sm_data5 = shmat(shmid5, NULL, 0)) == (i_node *)-1) {
        perror("shmat");
        exit(1);
    }

	printf("Unmounting disk...\n");

	int fd = open(NAME_OF_VIRTUAL_DISK, O_WRONLY);

	int sb_size = sizeof(superblock);													//mege8os se bytes
	int bbm_size = sm_data1->used_blocks_for_block_bmap * sm_data1->block_size;			//mege8os se bytes
	int ibm_size = sm_data1->used_blocks_for_inodes_bmap * sm_data1->block_size;		//mege8os se bytes
	int dt_size = sm_data1->used_blocks_for_dir_table * sm_data1->block_size;			//mege8os se bytes
	int ind_size = sm_data1->used_blocks_for_inodes * sm_data1->block_size;				//mege8os se bytes

	lseek(fd, 0L, 0);
	write(fd, sm_data1, sb_size);
	long pos = lseek(fd, sb_size, 0);
	write(fd, sm_data2, bbm_size);
	long pos2 = lseek(fd, pos+bbm_size, 0);
	write(fd, sm_data3, ibm_size);
	long pos3 = lseek(fd, pos2+ibm_size, 0);
	write(fd, sm_data4, dt_size);
	lseek(fd, pos3+dt_size, 0);
	write(fd, sm_data5, ind_size);

	close(fd);

	printf("Disk unmounted successfuly!\n");

	error1 = shmdt(sm_data1); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error1 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	error2 = shmdt(sm_data2); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error2 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	error3 = shmdt(sm_data3); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error3 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	error4 = shmdt(sm_data4); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error4 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	error5 = shmdt(sm_data5); //apokollhsh diergasias apo thn koinh mnhmh.
	if(error5 == -1) {
		printf("Could not detach from shared memory!\n");
		exit(1);
	}

	return 0;
}
