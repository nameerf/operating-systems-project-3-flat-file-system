#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "types.h"

#define ONE_BYTE 8

#define NAME_OF_VIRTUAL_DISK "virtual_disk.dat"

int create_block_bit_map(void);
int create_inodes_bit_map(void);
int create_directory_table(directory_table []);
int create_inodes(i_node []);

superblock *sb = NULL;
unsigned char *bbm = NULL;
unsigned char *ibm = NULL;
directory_table *dt = NULL;
i_node *ind = NULL;

int number_of_blocks;
int block_size;

int main(int argc, char *argv[]) {

	if(argc == 2) {
		//int i;
		//for(i=0; i<argc; i++)
		//	printf("argv[%d] = %s\n", i, argv[i]);

		number_of_blocks = atoi(argv[0]);
		block_size = atoi(argv[1]);

		/* UNCOMMENT AND FINILIZE AT THE END */
		/*
		if(number_of_blocks < 1024 || number_of_blocks > 30720) {
			printf("You must enter a number between 1024 and 30720 for the blocks.\n");
			return 0;
		}
		if(block_size < 32 || block_size > 1024) {
			printf("You must enter a number between 32 and 1024 for the size of each block.\n");
			printf("Also the size the block must be a power of 2 (ex. 32, 64, 128).\n");
			return 0;
		}
		*/

		directory_table dir_tbl[number_of_blocks];
		i_node inds[number_of_blocks];

		int sb_size = sizeof(superblock);					//mege8os se bytes
		int bbm_size = create_block_bit_map();				//mege8os se bytes
		int ibm_size = create_inodes_bit_map();				//mege8os se bytes
		int dt_size = create_directory_table(dir_tbl);		//mege8os se bytes
		int ind_size = create_inodes(inds);					//mege8os se bytes
		int data_size = number_of_blocks * block_size;		//mege8os se bytes
		int virtual_disk_size = sb_size + bbm_size + ibm_size + dt_size + ind_size + data_size;		//mege8os se bytes


		/*
		printf("Superblock size (in bytes) is %d\n", sizeof(superblock));
		printf("Directory table size (in bytes) is: %d\n", sizeof(directory_table));
		printf("I-node size (in bytes) is: %d\n\n", sizeof(i_node));

		printf("Superblock size (in blocks) is %d\n", 1);
		printf("Superblock size (in bytes) is %d\n", sizeof(superblock));
		printf("Block bit map size (in blocks) is: %d\n", bbm_blocks);
		printf("Block bit map size (in bytes) is: %d\n", bbm_size);
		printf("I-node bit map size (in blocks) is: %d\n", ibm_blocks);
		printf("I-node bit map size (in bytes) is: %d\n", ibm_size);
		printf("Directory table size (in blocks) is: %d\n", dt_blocks);
		printf("Directory table size (in bytes) is: %d\n", dt_size);
		printf("I-nodes size (in blocks) is: %d\n", ind_blocks);
		printf("I-nodes size (in bytes) is: %d\n", ind_size);
		printf("Data size (in blocks) is: %d\n", number_of_blocks);
		printf("Data size (in bytes) is: %d\n", data_size);
		printf("Disk size (in bytes) is: %d\n\n", disk_size);
		*/

		sb = (superblock*)malloc(sb_size);
		sb->num_of_inodes = number_of_blocks;
		sb->block_size = block_size;
		sb->used_blocks_for_block_bmap = bbm_size / block_size;		//mege8os se blocks
		sb->used_blocks_for_inodes_bmap = ibm_size / block_size;	//mege8os se blocks
		sb->used_blocks_for_dir_table = dt_size / block_size;		//mege8os se blocks
		sb->used_blocks_for_inodes = ind_size / block_size;			//mege8os se blocks
		sb->number_of_data_blocks = number_of_blocks;

		int fd = open(NAME_OF_VIRTUAL_DISK, O_WRONLY | O_TRUNC);

		printf("Please wait while the file system is being created and initialized...\n");
		int i;
		for(i=0; i<virtual_disk_size; i++) {
			write(fd, " ", 1);
		}

		lseek(fd, 0L, 0);
		write(fd, sb, sb_size);
		long pos = lseek(fd, sb_size, 0);
		write(fd, bbm, bbm_size);
		long pos2 = lseek(fd, pos+bbm_size, 0);
		write(fd, ibm, ibm_size);
		long pos3 = lseek(fd, pos2+ibm_size, 0);
		write(fd, dt, dt_size);
		lseek(fd, pos3+dt_size, 0);
		write(fd, ind, ind_size);

		printf("File system created and initialized successfuly!\n");

		// Deallocate!
		free(sb);
		free(bbm);
		free(ibm);

		close(fd);
	}

	return 0;
}

int create_block_bit_map(void) {
	int num_of_elements = number_of_blocks/ONE_BYTE;

	void *_tmp = realloc(bbm, num_of_elements*sizeof(char));

	// If the reallocation didn't go so well, inform the user and bail out
	if(!_tmp) {
		fprintf(stderr, "ERROR: Couldn't realloc memory!\n");
		exit(-1);
	}

	bbm = (unsigned char*)_tmp;

	int i;
	for(i=0; i<num_of_elements; i++)
		bbm[i] = 0;

	bbm[3] = 3;
	int size = sizeof(bbm) * num_of_elements;		//mege8os se bytes

	return size;
}

int create_inodes_bit_map(void) {
	int num_of_elements = number_of_blocks/ONE_BYTE;

	void *_tmp = realloc(ibm, num_of_elements*sizeof(char));

	// If the reallocation didn't go so well, inform the user and bail out
	if(!_tmp) {
		fprintf(stderr, "ERROR: Couldn't realloc memory!\n");
		exit(-1);
	}

	ibm = (unsigned char*)_tmp;

	int i;
	for(i=0; i<num_of_elements; i++)
		ibm[i] = 0;

	ibm[2] = 2;
	int size = sizeof(ibm) * num_of_elements;	//mege8os se bytes

	return size;
}

int create_directory_table(directory_table dir_table[]) {
	int i;
	for(i=0; i<number_of_blocks; i++) {
		memset(dir_table[i].filename, 0, sizeof(dir_table[i].filename));
		dir_table[i].inode_index = -1;
	}

	strcpy(dir_table[5].filename, "hello_world");
	dir_table[5].inode_index = 44;

	dt = dir_table;
	int size = sizeof(directory_table) * number_of_blocks;	//mege8os se bytes

	return size;
}

int create_inodes(i_node inodes[]) {
	int i;
	for(i=0; i<number_of_blocks; i++) {
		inodes[i].file_size = 101;
		memset(inodes[i].indexed_blocks, 0, sizeof(inodes[i].indexed_blocks));
	}

	inodes[3].indexed_blocks[7] = 55;

	ind = inodes;
	int size = sizeof(i_node) * number_of_blocks;	//mege8os se bytes

	return size;
}
