#include <stdlib.h>
#include <stdio.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/ipc.h>		/* gia thn koinh mnhmh. */
#include <sys/shm.h>		/* gia thn koinh mnhmh. */

#include "types.h"

#define SHM_KEY_1 26151		/* to "kleidi 1" ths koinhs mnhmhs. */
#define SHM_KEY_2 26152		/* to "kleidi 2" ths koinhs mnhmhs. */
#define SHM_KEY_3 26153		/* to "kleidi 3" ths koinhs mnhmhs. */
#define SHM_KEY_4 26154		/* to "kleidi 4" ths koinhs mnhmhs. */
#define SHM_KEY_5 26155		/* to "kleidi 4" ths koinhs mnhmhs. */
#define SHM_SIZE_1 128		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */
#define SHM_SIZE_2 1024		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */
#define SHM_SIZE_3 8192		/* to mege8os pou desmeuoyme gia thn koinh mnhmh. */

#define FALSE 0
#define TRUE 1

#define MAX_COMMAND_LENGTH 30
#define MAX_PARAMETER_LENGTH 10
#define MAX_PARAMETERS 5
#define MAX_PATHS 5

#define NUM_OF_COMMANDS 12

void type_prompt(void);
void read_command(char [], char [MAX_PARAMETERS][MAX_PARAMETER_LENGTH], int *);

char buffer[100];
int shm_id1, shm_id2, shm_id3, shm_id4, shm_id5;

int main(void) {

	int status;
	int fs_initialized = FALSE;
	int mounted = FALSE;
	int valid_command = FALSE;

    /* Create the first segment. */
	if((shm_id1 = shmget(SHM_KEY_1, SHM_SIZE_1, 0666 | IPC_CREAT)) < 0) {
		printf("could not create shared memory!\n");
		exit(1);
	}

	/* Create the second segment. */
	if((shm_id2 = shmget(SHM_KEY_2, SHM_SIZE_2, 0666 | IPC_CREAT)) < 0) {
		printf("could not create shared memory!\n");
		exit(1);
	}

	/* Create the third segment. */
	if((shm_id3 = shmget(SHM_KEY_3, SHM_SIZE_2, 0666 | IPC_CREAT)) < 0) {
		printf("could not create shared memory!\n");
		exit(1);
	}

	/* Create the fourth segment. */
	if((shm_id4 = shmget(SHM_KEY_4, SHM_SIZE_3, 0666 | IPC_CREAT)) < 0) {
		printf("could not create shared memory!\n");
		exit(1);
	}

	/* Create the fifth segment. */
	if((shm_id5 = shmget(SHM_KEY_5, SHM_SIZE_3, 0666 | IPC_CREAT)) < 0) {
		printf("could not create shared memory!\n");
		exit(1);
	}

	const char *available_commands[] = {"mkfs", "mount", "umount", "quit", "help", "ls", "cp", "mv", "rm", "df", "cat", "echo"};

	char current_directory[] = "./";

	char *command;
	char *parameters[MAX_PARAMETERS];

	char com[MAX_COMMAND_LENGTH];
	char par[MAX_PARAMETERS][MAX_PARAMETER_LENGTH];

	int x = 0;
	int *num_of_par;
	num_of_par = &x;

	while(TRUE) {
		type_prompt();

		read_command(com, par, num_of_par);

		command = com;

		int i;
		for(i=0; i<5; i++) {
			parameters[i] = NULL;
			if(i < *num_of_par)
				parameters[i] = par[i];
		}

		if(fork() != 0) {
			/* kodikas mhtrikhs diergasias */
			if(strcmp("mkfs", command) == 0)
				fs_initialized = TRUE;
			if(strcmp("mount", command) == 0 && fs_initialized)
				mounted = TRUE;

			waitpid(-1, &status, 0);
		}
		else {
			/*kodikas 8ygatrikhs diergasias */

			/*
			printf("command is %s#\n", command);
			printf("1st parameter is %s#\n", parameters[0]);
			printf("2nd parameter is %s#\n", parameters[1]);
			printf("3rd parameter is %s#\n", parameters[2]);
			printf("4th parameter is %s#\n", parameters[3]);
			printf("5th parameter is %s#\n", parameters[4]);
			*/

			int i;
			for(i=0; i<NUM_OF_COMMANDS; i++) {
				if(strcmp(available_commands[i], command) == 0) {
					valid_command = TRUE;
					break;
				}
			}

			if(!valid_command) {
				printf("Unknown command!\n");
				exit(0);
			}

			if(strcmp("help", command) == 0 || strcmp("mkfs", command) == 0)
				execve(strcat(current_directory, command), parameters, 0);
			else if(strcmp("mount", command) == 0) {
				if(fs_initialized)
					execve(strcat(current_directory, command), parameters, 0);
				else {
					printf("Command cannot execute because filesystem isn't initialized yet.\n");
					printf("Initialize the file system with the 'mkfs' command first!\n");
				}
			}
			else {
				if(mounted)
					execve(strcat(current_directory, command), parameters, 0);
				else {
					if(!fs_initialized) {
						printf("Command cannot execute because filesystem isn't initialized yet.\n");
						printf("Initialize the file system with the 'mkfs' command and then mount it\n");
						printf("with the 'mount' command to be able to enter other commands.\n");
					}
					else {
						printf("Command cannot execute because filesystem isn't mounted.\n");
						printf("Mount the file system with the 'mount' command and retry.\n");
					}
				}
			}

			exit(0);
		}
	}

	return 0;
}

void type_prompt(void) {
	printf("[my shell]# ");
}

void read_command(char com[], char param[MAX_PARAMETERS][MAX_PARAMETER_LENGTH], int *nop) {

	fgets(buffer, 100, stdin);

	int command_completed = FALSE;
	int j = 0;
	int k = 0;
	int m = 0;
	int i;
	for(i=0; i<strlen(buffer); i++) {
		if(!command_completed)
			com[j++] = buffer[i];
		if(i == strlen(buffer)-1 && !command_completed) {
			com[j-1] = '\0';
			break;
		}
		if(buffer[i] == ' ' && !command_completed) {
			com[j-1] = '\0';
			command_completed = TRUE;
			i++;
		}
		if(command_completed) {
			if(m < MAX_PARAMETERS) {
				param[m][k++] = buffer[i];
				if(buffer[i] == ' ') {
					param[m][k-1] = '\0';
					m++;
					k = 0;
				}
				if(i == strlen(buffer)-1) {
					param[m][k-1] = '\0';
					break;
				}
			}
			else {
				printf("Cannot take more than 5 parameters!\n");
				break;
			}
		}
	}

	if(command_completed)
		*nop = m+1;
	else
		*nop = 0;

	if(strcmp(com, "quit") == 0) {
		shmctl(shm_id1, IPC_RMID, NULL); //elegxos-diagrafh ths koinhs mnhmhs.
		shmctl(shm_id2, IPC_RMID, NULL); //elegxos-diagrafh ths koinhs mnhmhs.
		shmctl(shm_id3, IPC_RMID, NULL); //elegxos-diagrafh ths koinhs mnhmhs.
		shmctl(shm_id4, IPC_RMID, NULL); //elegxos-diagrafh ths koinhs mnhmhs.
		shmctl(shm_id5, IPC_RMID, NULL); //elegxos-diagrafh ths koinhs mnhmhs.
		exit(0);
	}
}
